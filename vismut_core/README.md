# Vismut Core
A library for node based image editing created for use in [Vismut](https://gitlab.com/vismutorg/vismut).

It's not meant to be used by third parties yet, but you *can* use it and it should be easy to see how it works by looking at the tests in `tests/integration_tests.rs`.

## Goal
The current goal is to support the Vismut application.

## Features
- Multithreaded, each node is executed in its own thread
- Nested graphs, a single node can contain an entire graph, so you can reuse graphs
- Basic nodes.
- Every image channel is 32 bit float
- Priority system to control the order of node calculation

## License
Vismut Core is licensed under either of

 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)
 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)

at your option. This means you can select the license you prefer! This dual-licensing approach is the de-facto standard in the Rust ecosystem and there are [very good reasons](https://github.com/bevyengine/bevy/issues/2373) to include both.

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any additional terms or conditions.
