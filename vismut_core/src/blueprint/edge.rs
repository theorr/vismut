use crate::address::{NodeId, SlotId};
use crate::edge::Edge;
use std::fmt::{Display, Formatter};

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct BlueprintEdge {
    pub node_id_output: NodeId,
    pub slot_id_output: SlotId,
    pub node_id_input: NodeId,
    pub slot_id_input: SlotId,
}

impl Display for BlueprintEdge {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "(OUTPUT node id: {}, slot id: {}, INPUT node id: {} slot id: {})",
            self.node_id_output, self.slot_id_output, self.node_id_input, self.slot_id_input
        )
    }
}

impl From<Edge> for BlueprintEdge {
    fn from(edge: Edge) -> Self {
        Self {
            node_id_output: edge.slot_address_output.node_id,
            slot_id_output: edge.slot_address_output.slot_id,
            node_id_input: edge.slot_address_input.node_id,
            slot_id_input: edge.slot_address_input.slot_id,
        }
    }
}

impl BlueprintEdge {
    pub const fn new(
        node_id_output: NodeId,
        slot_id_output: SlotId,
        node_id_input: NodeId,
        slot_id_input: SlotId,
    ) -> Self {
        Self {
            node_id_output,
            slot_id_output,
            node_id_input,
            slot_id_input,
        }
    }
}
