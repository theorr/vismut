use std::fmt::{Display, Formatter};

/// The largest allowed power, is here to save computers from running out of RAM due to a single
/// buffer that's too large.
pub(crate) const MAX_POW: u8 = 15;

/// Represents a power of two.
///
/// Ensures the power stays below `MAX_POW`.
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct Pow2(u8);

impl Default for Pow2 {
    fn default() -> Self {
        Self::new(0)
    }
}

impl Display for Pow2 {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "(2^{} = {})", self.inner(), self.result())
    }
}

impl Pow2 {
    /// If it is created with a `u8` larger than `MAX_POW`, it gets clamped to `MAX_POW`.
    pub fn new<T: Into<u8>>(power: T) -> Self {
        let power = power.into();
        assert!(power <= MAX_POW, "tried creating a `Pow2` above `MAX_POW`");
        Self(power)
    }

    /// Turns a regular number into its corresponding power of two.
    ///
    /// Returns `None` if there is no matching power of two.
    pub(crate) fn log_2<T: Into<usize>>(value: T) -> Option<Self> {
        let value = value.into();
        for i in 0..=MAX_POW {
            if value == 2_usize.pow(i as u32) {
                return Some(Self(i));
            }
        }

        None
    }

    /// Adds the given value, but will not return a `Pow2` above `MAX_POW`.
    pub fn saturating_add(&self, pow_2_relative: Pow2Relative) -> Self {
        fn clamp(input: u8, min: u8, max: u8) -> u8 {
            if input < min {
                min
            } else if input > max {
                max
            } else {
                input
            }
        }

        let new_pow2 = clamp((self.0 as i8 + pow_2_relative.inner()) as u8, 0, MAX_POW);
        Self(new_pow2)
    }

    /// Returns the difference between two `Pow2` as a `Pow2Relative`.
    pub fn difference(&self, other: Self) -> Pow2Relative {
        let difference = self.0 as i8 - other.0 as i8;
        Pow2Relative::new(difference)
    }

    /// Returns the inner `u8` of the `Pow2`.
    pub fn inner(&self) -> u8 {
        self.0
    }

    /// Calculates `2^self` and returns the result.
    pub fn result(&self) -> usize {
        2_usize.pow(self.0 as u32)
    }
}

/// A power that can be used to "alter" a `Pow2`. This type exists to avoid confusion with regular
/// `i8` types.
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct Pow2Relative(i8);

impl Display for Pow2Relative {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Default for Pow2Relative {
    fn default() -> Self {
        Self::new(0)
    }
}

impl Pow2Relative {
    pub fn new(relative_power: i8) -> Self {
        assert!(
            relative_power.abs() as u8 <= MAX_POW,
            "tried creating a `Pow2Relative` larger than `MAX_POW`"
        );

        Self(relative_power)
    }

    /// Returns the inner `i8` of the `Pow2Relative`.
    pub fn inner(&self) -> i8 {
        self.0
    }
}

/// Represents a size constrained to a power of two.
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct SizePow2 {
    width: Pow2,
    height: Pow2,
}

impl Default for SizePow2 {
    fn default() -> Self {
        Self::new(Pow2(0), Pow2(0))
    }
}

impl Display for SizePow2 {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "[ {}, {} ]", self.width, self.height)
    }
}

impl SizePow2 {
    pub fn new(width: Pow2, height: Pow2) -> Self {
        Self { width, height }
    }

    /// Returns the power of the width.
    pub fn width(&self) -> Pow2 {
        self.width
    }

    /// Returns the power of the height.
    pub fn height(&self) -> Pow2 {
        self.height
    }

    /// Returns the powers of the width and height in a tuple
    pub fn to_tuple(self) -> (Pow2, Pow2) {
        (self.width, self.height)
    }

    /// Returns the powers of width and height as a tuple of `u8` values.
    pub fn inner(&self) -> (u8, u8) {
        (self.width.inner(), self.height.inner())
    }

    /// Calculates `2^self` for width and height and returns the result in a tuple.
    pub fn result(&self) -> (usize, usize) {
        (self.width.result(), self.height.result())
    }

    /// Add the given `Pow2Relative`, will not go over `MAX_POW` while maintaining the aspect ratio.
    pub fn saturating_add(&self, pow_2_relative: Pow2Relative) -> Self {
        let pow_2_relative = {
            let min = -(self.width.0.min(self.height.0) as i8);

            let largest_pow = self.width.0.max(self.height.0);
            let max = (MAX_POW - largest_pow) as i8;

            let clamped_power = pow_2_relative.0.clamp(min, max);
            Pow2Relative::new(clamped_power)
        };

        Self {
            width: self.width.saturating_add(pow_2_relative),
            height: self.height.saturating_add(pow_2_relative),
        }
    }

    /// Returns the difference on each axis.
    pub fn difference(&self, other: Self) -> (Pow2Relative, Pow2Relative) {
        let width_difference = self.width.difference(other.width);
        let height_difference = self.height.difference(other.height);
        (width_difference, height_difference)
    }

    /// Returns the number of pixels this size would cover.
    pub fn pixel_count(&self) -> usize {
        let (width, height) = self.result();
        width * height
    }
}

/// Returns the closest result of a power of two.
pub fn closest_pow_2(input: u32) -> u32 {
    let valid_values = (0..=MAX_POW).map(|pow| 2_u32.pow(pow as u32));

    let mut closest_value = 0;
    let mut smallest_difference = input;
    for value in valid_values {
        let difference = (value as i64 - input as i64).abs() as u32;
        if difference <= smallest_difference {
            closest_value = value;
            smallest_difference = difference;
        }
    }

    closest_value
}
