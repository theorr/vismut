use crate::error::{Result, VismutError};
use crate::prelude::*;
use std::fmt::{Display, Formatter};

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum SlotType {
    Gray,
    Rgba,
}

impl Default for SlotType {
    fn default() -> Self {
        Self::Gray
    }
}

impl Display for SlotType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let text = if let Self::Gray = self {
            "gray"
        } else {
            "rgba"
        };

        write!(f, "{}", text)
    }
}

impl SlotType {
    pub fn compatible(&self, other: Self) -> Result<()> {
        if *self == other {
            Ok(())
        } else {
            Err(VismutError::IncompatibleSlotTypes(*self, other))
        }
    }
}

#[derive(Clone, Debug)]
pub struct Slot {
    pub slot_type: SlotType,
    pub slot_id: SlotId,
    pub name: String,
}

impl Slot {
    pub fn new(slot_type: SlotType, slot_id: SlotId, name: &str) -> Self {
        Self {
            slot_type,
            slot_id,
            name: name.to_string(),
        }
    }
}
