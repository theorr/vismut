use crate::address::{Side, SlotAddress, SlotAddressSide};
use crate::error::{Result, VismutError};
use std::fmt::{Display, Formatter};

#[derive(Debug, Copy, Clone, Default, Eq, PartialEq)]
pub struct Edge {
    pub slot_address_output: SlotAddress,
    pub slot_address_input: SlotAddress,
}

impl Display for Edge {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "(output: {}, input: {})",
            self.slot_address_output, self.slot_address_input
        )
    }
}

impl Edge {
    pub const fn new(slot_address_output: SlotAddress, slot_address_input: SlotAddress) -> Self {
        Self {
            slot_address_output,
            slot_address_input,
        }
    }

    pub fn from_sided(
        slot_address_side_a: SlotAddressSide,
        slot_address_side_b: SlotAddressSide,
    ) -> Result<Self> {
        if slot_address_side_a.node_id == slot_address_side_b.node_id {
            Err(VismutError::InvalidNodeId(slot_address_side_a.node_id))
        } else if slot_address_side_a.side == slot_address_side_b.side {
            Err(VismutError::InvalidSide)
        } else {
            let (slot_address_output, slot_address_input) =
                if let Side::Output = slot_address_side_a.side {
                    (
                        slot_address_side_a.without_side(),
                        slot_address_side_b.without_side(),
                    )
                } else {
                    (
                        slot_address_side_b.without_side(),
                        slot_address_side_a.without_side(),
                    )
                };

            Ok(Self {
                slot_address_output,
                slot_address_input,
            })
        }
    }
}
