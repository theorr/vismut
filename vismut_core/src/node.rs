use crate::error::{Result, VismutError};
use crate::prelude::*;
use crate::resize::Resize;
use crate::slot::{Slot, SlotType};
use std::fmt::{Display, Formatter};
use std::path::PathBuf;

/// This enum is the public interface for nodes. It makes it friendly and easy to edit the node
/// dag. When `Engine.prepare()` is executed before the node dag gets processed, these nodes are
/// transformed into `LiveNodeType`s, which can actually be processed. One `NodeType` can be
/// transformed into several `LiveNodeType`s.
#[derive(Clone, Debug, PartialEq)]
pub enum NodeType {
    /// Reads the specified image from disk. If the image's size is not a power of two it will be
    /// resized to the closest power of two on each axis.
    Image(PathBuf),
    OutputRgba(String),
    /// Creates a new grayscale image containing one pixel of the given value.
    Grayscale(f32),
    /// Takes one rgba image and splits it into 4 grayscale images.
    SplitRgba(Resize),
    /// Takes 4 grayscale images and combines them into one rgba image.
    MergeRgba(Resize),
}

impl Display for NodeType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let text = match self {
            Self::Image(path) => format!("Image({})", path.display()),
            Self::OutputRgba(name) => format!("OutputRgba({})", name),
            Self::Grayscale(value) => format!("Grayscale({:.3})", value),
            Self::SplitRgba(resize) => format!("SplitRgba({})", resize),
            Self::MergeRgba(resize) => format!("MergeRgba({})", resize),
        };

        write!(f, "{}", text)
    }
}

impl NodeType {
    pub fn name(&self) -> Result<&String> {
        if let Self::OutputRgba(name) = self {
            Ok(name)
        } else {
            Err(VismutError::IncompatibleNodeType(self.clone()))
        }
    }

    /// If the `NodeType` has a name, this function sets it to the input `String`.
    pub(crate) fn set_name(&mut self, new_name: String) -> Result<()> {
        let current_name = if let Self::OutputRgba(name) = self {
            name
        } else {
            return Err(VismutError::IncompatibleNodeType(self.clone()));
        };

        *current_name = new_name;

        Ok(())
    }

    pub fn slot_id_exists(&self, side: Side, slot_id: SlotId) -> Result<()> {
        let slots = self.slots(side);

        if slots.iter().any(|slot| slot.slot_id == slot_id) {
            Ok(())
        } else {
            Err(VismutError::InvalidSlotId(slot_id))
        }
    }

    pub fn eq_discriminant(&self, other: &Self) -> bool {
        std::mem::discriminant(self) == std::mem::discriminant(other)
    }

    pub fn slots(&self, side: Side) -> Vec<Slot> {
        if let Side::Input = side {
            match self {
                Self::Image(..) => Vec::new(),
                Self::OutputRgba(..) => vec![Slot::new(SlotType::Rgba, SlotId(0), "input")],
                Self::Grayscale(..) => Vec::new(),
                Self::SplitRgba(..) => {
                    vec![Slot::new(SlotType::Rgba, SlotId(0), "input")]
                }
                Self::MergeRgba(..) => {
                    vec![
                        Slot::new(SlotType::Gray, SlotId(0), "red"),
                        Slot::new(SlotType::Gray, SlotId(1), "green"),
                        Slot::new(SlotType::Gray, SlotId(2), "blue"),
                        Slot::new(SlotType::Gray, SlotId(3), "alpha"),
                    ]
                }
            }
        } else {
            match self {
                Self::Image(..) => vec![Slot::new(SlotType::Rgba, SlotId(0), "output")],
                Self::OutputRgba(..) => Vec::new(),
                Self::Grayscale(..) => vec![Slot::new(SlotType::Gray, SlotId(0), "output")],
                Self::SplitRgba(..) => {
                    vec![
                        Slot::new(SlotType::Gray, SlotId(0), "red"),
                        Slot::new(SlotType::Gray, SlotId(1), "green"),
                        Slot::new(SlotType::Gray, SlotId(2), "blue"),
                        Slot::new(SlotType::Gray, SlotId(3), "alpha"),
                    ]
                }
                Self::MergeRgba(..) => vec![Slot::new(SlotType::Rgba, SlotId(0), "output")],
            }
        }
    }

    /// Returns the given `Slot` if it exists on this type.
    pub fn slot_with_id(&self, side: Side, slot_id: SlotId) -> Result<Slot> {
        let slots = self.slots(side);
        slots
            .into_iter()
            .find(|slot| slot.slot_id == slot_id)
            .ok_or(VismutError::InvalidSlotId(slot_id))
    }

    pub fn resize(&self) -> Option<Resize> {
        match self {
            NodeType::SplitRgba(resize) | NodeType::MergeRgba(resize) => Some(*resize),
            _ => None,
        }
    }

    pub fn resize_mut(&mut self) -> Option<&mut Resize> {
        match self {
            NodeType::SplitRgba(resize) | NodeType::MergeRgba(resize) => Some(resize),
            _ => None,
        }
    }

    pub(crate) fn slot_from_id(&self, side: Side, slot_id: SlotId) -> Result<Slot> {
        self.slots(side)
            .into_iter()
            .find(|slot| slot.slot_id == slot_id)
            .ok_or(VismutError::InvalidSlotId(slot_id))
    }
}
