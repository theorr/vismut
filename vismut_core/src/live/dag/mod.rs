mod flatten;

use crate::address::{DagId, NodeAddress, NodeId, Side, SlotAddress, SlotAddressSide, SlotId};
use crate::engine::NodeState;
use crate::error::{Result, VismutError};
use crate::live::address::{LiveNodeId, LiveSlotAddress, LiveSlotAddressSide};
use crate::live::dag::flatten::FlatEdge;
use crate::live::edge::LiveEdge;
use crate::live::error::{LiveError, LiveResult};
use crate::live::node::{FinishedProcessing, LiveNode, ProcessData};
use crate::live::slot_data::{SlotData, SlotImage};
use crate::pow_two::SizePow2;
use flatten::FlatBlueprint;
use std::collections::{BTreeMap, BTreeSet};
use std::num::NonZeroUsize;
use std::sync::{mpsc, Mutex};
use std::thread;
use std::time::Duration;
use threadpool::ThreadPool;

#[derive(Debug)]
struct WaitingSender(pub Mutex<mpsc::Sender<(SizePow2, Vec<u8>)>>);

#[derive(Debug)]
pub(crate) struct LiveDag {
    blueprint_dags_compare: FlatBlueprint,
    slot_address_map: BTreeMap<SlotAddressSide, LiveSlotAddressSide>,
    live_nodes: Vec<LiveNode>,
    live_edges: Vec<LiveEdge>,
    node_states: Vec<(LiveNodeId, NodeState)>,
    processing_node_ids: BTreeSet<LiveNodeId>,
    slot_data: Vec<SlotData>,
    waiting_senders: BTreeMap<LiveSlotAddress, Vec<WaitingSender>>,
    sender: Mutex<mpsc::Sender<FinishedProcessing>>,
    receiver: Mutex<mpsc::Receiver<FinishedProcessing>>,
    thread_pool: Mutex<ThreadPool>,
    live_node_counter: usize,
}

impl LiveDag {
    pub fn new() -> Self {
        let (sender, receiver) = mpsc::channel();
        let num_threads = thread::available_parallelism()
            .unwrap_or(NonZeroUsize::new(1).unwrap())
            .get();

        Self {
            blueprint_dags_compare: FlatBlueprint::new(),
            slot_address_map: BTreeMap::new(),
            live_nodes: Vec::new(),
            live_edges: Vec::new(),
            node_states: Vec::new(),
            processing_node_ids: BTreeSet::new(),
            slot_data: Vec::new(),
            waiting_senders: BTreeMap::new(),
            sender: Mutex::new(sender),
            receiver: Mutex::new(receiver),
            thread_pool: Mutex::new(ThreadPool::new(num_threads)),
            live_node_counter: 0,
        }
    }

    /// Removes all `LiveNode`s and any connected `LiveEdge`s that were created from the given
    /// `NodeAddress`.
    fn remove_live_nodes(&mut self, node_address: NodeAddress) {
        let (indices_to_remove, live_node_ids): (Vec<_>, Vec<_>) = self
            .live_nodes
            .iter()
            .enumerate()
            .filter(|(_, live_node)| live_node.creator_address == node_address)
            .map(|(i, live_node)| (i, live_node.live_node_id))
            .unzip();

        for i in indices_to_remove.into_iter().rev() {
            self.live_nodes.swap_remove(i);
        }

        for live_node_id in live_node_ids {
            let node_state_indices = self
                .node_states
                .iter()
                .enumerate()
                .filter(|(_, (state_live_node_id, _))| *state_live_node_id == live_node_id)
                .map(|(i, _)| i)
                .collect::<Vec<_>>();

            for i in node_state_indices.iter().rev() {
                self.node_states.swap_remove(*i);
            }

            let slot_data_indices = self.slot_data_indices(live_node_id);
            for i in slot_data_indices.iter().rev() {
                self.slot_data.swap_remove(*i);
            }

            let slot_address_mappings = self
                .slot_address_map
                .keys()
                .filter(|slot_address_side| slot_address_side.node_address() == node_address)
                .cloned()
                .collect::<Vec<_>>();
            for slot_address_side in slot_address_mappings.iter() {
                self.slot_address_map.remove(slot_address_side);
            }

            #[allow(clippy::needless_collect)]
            let live_edges_indices = self
                .live_edges
                .iter()
                .enumerate()
                .filter(|(_, live_edge)| {
                    live_edge.output.live_node_id == live_node_id
                        || live_edge.input.live_node_id == live_node_id
                })
                .map(|(index, ..)| index)
                .collect::<Vec<_>>();
            for index in live_edges_indices.into_iter().rev() {
                self.live_edges.swap_remove(index);
            }
        }
    }

    /// Restores the `LiveEdges` that should be connected to all `LiveNode`s of the given
    /// `NodeAddress`. This is used when a node has been changed, it's needed since removing and
    /// then adding a node changes all the `LiveNodeId`s.
    ///
    /// "Activating" a node already creates all "internal" `LiveEdge`s, so this takes care of all
    /// other `LiveEdge`s.
    fn restore_edges(&mut self, node_address: NodeAddress, fresh_flat_edges: &[FlatEdge]) {
        let flat_edges = fresh_flat_edges.iter().filter(|flat_edge| {
            flat_edge.input.without_slot_id() == node_address
                || flat_edge.output.without_slot_id() == node_address
        });

        for edge in flat_edges {
            let output = self
                .slot_address_map
                .get(&edge.output.with_side(Side::Output))
                .expect("this mapping should exist")
                .without_side();
            let input = self
                .slot_address_map
                .get(&edge.input.with_side(Side::Input))
                .expect("this mapping should exist")
                .without_side();

            let live_edge = LiveEdge { output, input };
            self.live_edges.push(live_edge);
        }
    }

    fn slot_data_indices(&self, live_node_id: LiveNodeId) -> Vec<usize> {
        self.slot_data
            .iter()
            .enumerate()
            .filter(|(_, slot_data)| {
                slot_data.live_slot_address_sideless.live_node_id == live_node_id
            })
            .map(|(i, _)| i)
            .collect::<Vec<_>>()
    }

    fn invalidate_node(&mut self, live_node_id: LiveNodeId) -> LiveResult<()> {
        *self.node_state_mut(live_node_id)? = NodeState::Dirty;

        let slot_data_indices = self.slot_data_indices(live_node_id);
        for index in slot_data_indices.into_iter().rev() {
            self.slot_data.swap_remove(index);
        }

        Ok(())
    }

    /// Starts processing any nodes that can be processed.
    pub(crate) fn run(&mut self) {
        self.process_messages();

        for live_node_id in self.processable_ids() {
            assert!(
                self.processing_node_ids.insert(live_node_id),
                "attempted to start processing a node that was already in `processing_node_ids`"
            );

            self.set_node_state(live_node_id, NodeState::Processing)
                .expect("could not set NodeState");

            if let (Ok(slot_images), Ok(live_node)) =
                (self.node_inputs(live_node_id), self.live_node(live_node_id))
            {
                let process_data = ProcessData {
                    sender: self.sender.lock().unwrap().clone(),
                    live_node_id,
                    slot_images,
                };

                live_node
                    .live_node_type
                    .process(&self.thread_pool, process_data);
            } else {
                panic!("could not start processing node");
            }
        }
    }

    /// Handles all messages that come in from nodes that have finished processing.
    fn process_messages(&mut self) {
        loop {
            let message = self.receiver.lock().unwrap().try_recv();
            if let Ok(message) = message {
                let live_node_id = message.live_node_id;

                assert!(
                    self.processing_node_ids.remove(&live_node_id),
                    "got a `FinishedProcessing` message from a node not in the \
                    `processing_node_ids` list"
                );

                if self.live_node(live_node_id).is_err() {
                    // If the node does not exist, the result is not relevant.
                    continue;
                }
                if let Ok(NodeState::Dirty) = self.node_state(live_node_id) {
                    // If the node is dirty, the result is not relevant.
                    continue;
                }

                self.clear_nodes_data(live_node_id);

                for (slot_id, slot_image) in message.slot_images {
                    let live_slot_address = live_node_id.with_slot_id(slot_id);
                    let slot_data = SlotData {
                        live_slot_address_sideless: live_slot_address,
                        slot_image: slot_image.clone(),
                    };

                    if let Some(waiting_senders) = self.waiting_senders.remove(&live_slot_address) {
                        for sender in waiting_senders {
                            let sender = sender.0.into_inner().unwrap();
                            self.send_buffer(sender, slot_image.clone());
                        }
                    }

                    self.slot_data.push(slot_data);
                }

                self.set_node_state(live_node_id, NodeState::Clean)
                    .expect("`LiveNode` does not have corresponding `NodeState`");
            } else {
                break;
            }
        }
    }

    /// Processes until the entire dag is processed.
    pub fn run_until_done(&mut self) {
        while !self.done() {
            self.run();

            // Sleeping to avoid using 100% CPU while waiting.
            thread::sleep(Duration::from_micros(1));
        }
    }

    /// Returns a new unique `LiveNodeId`.
    fn new_id(&mut self) -> LiveNodeId {
        let id = self.live_node_counter;
        self.live_node_counter += 1;
        LiveNodeId(id)
    }

    /// Sets the number of threads to use for processing nodes. Note that reducing the count does
    /// not actually kill any threads, they will just sit unused.
    pub fn set_num_threads(&mut self, num_threads: NonZeroUsize) {
        self.thread_pool
            .lock()
            .unwrap()
            .set_num_threads(num_threads.get());
    }

    /// Returns the `SlotData` at the given `LiveSlotAddressSideless` if it exists.
    fn slot_data(&self, live_slot_address_sideless: LiveSlotAddress) -> Option<&SlotData> {
        self.slot_data.iter().find(|slot_data| {
            slot_data.live_slot_address_sideless.live_node_id
                == live_slot_address_sideless.live_node_id
                && slot_data.live_slot_address_sideless.slot_id
                    == live_slot_address_sideless.slot_id
        })
    }

    /// Returns all `NodeAddress`es of nodes that are in the given state. The output may contain
    /// duplicates and is unsorted.
    pub fn nodes_in_state(&self, node_state: NodeState) -> Vec<NodeAddress> {
        let live_node_ids = self.live_node_ids_in_state(node_state);

        self.slot_address_map
            .iter()
            .filter(|(.., live_slot_address_side)| {
                live_node_ids.contains(&live_slot_address_side.live_node_id)
            })
            .map(|(slot_address_side, ..)| slot_address_side.node_address())
            .collect::<Vec<_>>()
    }

    /// Returns all `NodeAddress`es of nodes that are NOT in the given state. The output may contain
    /// duplicates and is unsorted.
    pub fn nodes_not_in_state(&self, node_state: NodeState) -> Vec<NodeAddress> {
        let live_node_ids = self.live_node_ids_not_in_state(node_state);

        self.slot_address_map
            .iter()
            .filter(|(.., live_slot_address_side)| {
                live_node_ids.contains(&live_slot_address_side.live_node_id)
            })
            .map(|(slot_address_side, ..)| slot_address_side.node_address())
            .collect::<Vec<_>>()
    }

    /// Returns the `NodeState` of all `NodeId`s in the given DAG.
    pub fn node_states(&self, dag_id: DagId) -> BTreeMap<NodeId, NodeState> {
        let node_states = self
            .node_states
            .iter()
            .map(|(live_node_id, node_state)| {
                let node_address = self
                    .slot_address_map
                    .iter()
                    .find(|(_, live_slot_address_side)| {
                        live_slot_address_side.live_node_id == *live_node_id
                    })
                    .map(|(slot_address_side, _)| slot_address_side.node_address())
                    .expect("should always be able to find this");

                (node_address, node_state)
            })
            .filter(|(node_address, _)| node_address.dag_id == dag_id)
            .map(|(node_address, node_state)| (node_address.node_id, node_state));

        let mut output = BTreeMap::new();

        for (node_id, node_state) in node_states {
            let state = output.entry(node_id).or_insert(NodeState::Clean);
            *state = (*state).min(*node_state);
        }

        output
    }

    /// Returns all `LiveNodeId`s of nodes that are in the given state.
    fn live_node_ids_in_state(&self, node_state: NodeState) -> Vec<LiveNodeId> {
        self.node_states
            .iter()
            .filter(|(_, state_cmp)| *state_cmp == node_state)
            .map(|(id, _)| *id)
            .collect::<Vec<_>>()
    }

    /// Returns all `LiveNodeId`s of nodes that are NOT in the given state.
    fn live_node_ids_not_in_state(&self, node_state: NodeState) -> Vec<LiveNodeId> {
        self.node_states
            .iter()
            .filter(|(_, state_cmp)| *state_cmp != node_state)
            .map(|(id, _)| *id)
            .collect::<Vec<_>>()
    }

    /// Returns the `NodeId`s of nodes that are ready to be processed.
    fn processable_ids(&self) -> Vec<LiveNodeId> {
        let dirty = self.live_node_ids_in_state(NodeState::Dirty);
        let clean = self.live_node_ids_in_state(NodeState::Clean);

        dirty
            .into_iter()
            .filter(|live_node_id| {
                let parents = self.parent_ids(*live_node_id);
                parents.iter().all(|parent_id| clean.contains(parent_id))
                    && !self.processing_node_ids.contains(live_node_id)
            })
            .collect()
    }

    /// Returns all `LiveNodeId`s that are connected to the inputs on the given `LiveNodeId`.
    fn parent_ids(&self, live_node_id: LiveNodeId) -> Vec<LiveNodeId> {
        self.live_edges
            .iter()
            .filter(|live_edge| live_edge.input.live_node_id == live_node_id)
            .map(|live_edge| live_edge.output.live_node_id)
            .collect::<Vec<_>>()
    }

    /// Returns the RGBA buffer in the given slot.
    pub(crate) fn buffer_rgba(&self, slot_address: SlotAddress) -> Result<Vec<u8>> {
        let live_slot_address = self
            .slot_address_to_live(slot_address.with_side(Side::Output))?
            .without_side();

        let slot_image = self
            .slot_image(live_slot_address)
            .map_err(|_| VismutError::InvalidSlotAddress(slot_address))?;

        Ok(slot_image.to_u8())
    }

    fn send_buffer(&self, sender: mpsc::Sender<(SizePow2, Vec<u8>)>, slot_image: SlotImage) {
        self.thread_pool.lock().unwrap().execute(move || {
            let size = slot_image.size();
            let buffer = slot_image.to_u8();
            let _ = sender.send((size, buffer));
        });
    }

    /// Takes a sender where a buffer will be sent when it's ready, or right away if it already is.
    pub(crate) fn insert_waiting_sender(
        &mut self,
        sender: Mutex<mpsc::Sender<(SizePow2, Vec<u8>)>>,
        slot_address: SlotAddress,
    ) -> Result<()> {
        let live_slot_address = self
            .slot_address_to_live(slot_address.with_side(Side::Output))?
            .without_side();

        if let Ok(slot_image) = self.slot_image(live_slot_address) {
            self.send_buffer(sender.into_inner().unwrap(), slot_image.clone());
        } else {
            let waiting_senders = self
                .waiting_senders
                .entry(live_slot_address)
                .or_insert(Vec::new());
            waiting_senders.push(WaitingSender(sender));
        }

        Ok(())
    }

    /// Maps an input `SlotAddressSide` to its corresponding `LiveSlotAddressSide`.
    fn slot_address_to_live(
        &self,
        slot_address_side: SlotAddressSide,
    ) -> Result<LiveSlotAddressSide> {
        let live_node_id = self
            .slot_address_map
            .get(&slot_address_side)
            .ok_or_else(|| {
                VismutError::InvalidSlotAddressSide(
                    slot_address_side.without_side(),
                    slot_address_side.side,
                )
            })?;
        Ok(*live_node_id)
    }

    /// Returns the `SlotImage` in the given `LiveSlotAddressSideless`.
    fn slot_image(&self, live_slot_address_sideless: LiveSlotAddress) -> LiveResult<&SlotImage> {
        let slot_data = self.slot_data(live_slot_address_sideless).ok_or(
            LiveError::InvalidLiveSlotAddressSideless(live_slot_address_sideless),
        )?;
        Ok(&slot_data.slot_image)
    }

    /// Returns the size of the image in the given slot.
    pub(crate) fn slot_size(&self, slot_address: SlotAddress) -> Result<SizePow2> {
        let live_slot_address = self
            .slot_address_to_live(slot_address.with_side(Side::Output))?
            .without_side();
        let size = self
            .slot_image(live_slot_address)
            .map_err(|_| VismutError::InvalidSlotAddress(slot_address))?
            .size();
        Ok(size)
    }

    /// Returns all `SlotImage`s connected to any of the given `LiveNodeId`'s input slots.
    fn node_inputs(&self, live_node_id: LiveNodeId) -> LiveResult<Vec<(SlotId, SlotImage)>> {
        let live_node = self.live_node(live_node_id)?;
        let mut output_slot_images = Vec::new();

        for slot in live_node.live_node_type.slots(Side::Input) {
            let live_slot_address = live_node_id
                .with_slot_id(slot.slot_id)
                .with_side(Side::Input);

            if let Some(live_edge) = self.edges_in_slot(live_slot_address).first() {
                let slot_image = self.slot_image(live_edge.output)?.clone();
                output_slot_images.push((slot.slot_id, slot_image));
            }
        }

        Ok(output_slot_images)
    }

    /// Returns all `LiveEdge`s connected to the given `LiveSlotAddressSide`.
    fn edges_in_slot(&self, live_slot_address_side: LiveSlotAddressSide) -> Vec<LiveEdge> {
        self.live_edges
            .iter()
            .filter(|live_edge| {
                Side::Output == live_slot_address_side.side
                    && live_edge.output == live_slot_address_side.without_side()
                    || Side::Input == live_slot_address_side.side
                        && live_edge.input == live_slot_address_side.without_side()
            })
            .copied()
            .collect()
    }

    /// Sets the given `NodeAddress` to the given `NodeState`.
    fn set_node_state(
        &mut self,
        live_node_id: LiveNodeId,
        node_state: NodeState,
    ) -> LiveResult<()> {
        let node_state_mut = self.node_state_mut(live_node_id)?;
        *node_state_mut = node_state;
        Ok(())
    }

    /// Gets a reference to the given `NodeAddress`s `NodeState`.
    fn node_state(&self, live_node_id: LiveNodeId) -> LiveResult<&NodeState> {
        let (.., node_state) = self
            .node_states
            .iter()
            .find(|(address, _)| *address == live_node_id)
            .ok_or(LiveError::InvalidLiveNodeId(live_node_id))?;
        Ok(node_state)
    }

    /// Gets a mutable reference to the given `NodeAddress`s `NodeState`.
    fn node_state_mut(&mut self, live_node_id: LiveNodeId) -> LiveResult<&mut NodeState> {
        let (.., node_state) = self
            .node_states
            .iter_mut()
            .find(|(address, _)| *address == live_node_id)
            .ok_or(LiveError::InvalidLiveNodeId(live_node_id))?;
        Ok(node_state)
    }

    /// Removes all `NodeData`'s associated with the given `LiveNodeId` in `self.slot_data`.
    fn clear_nodes_data(&mut self, live_node_id: LiveNodeId) {
        #[allow(clippy::needless_collect)]
        let slot_data_indices = self
            .slot_data
            .iter()
            .enumerate()
            .filter(|(.., slot_data)| {
                slot_data.live_slot_address_sideless.live_node_id == live_node_id
            })
            .map(|(index, ..)| index)
            .collect::<Vec<_>>();

        for i in slot_data_indices.into_iter().rev() {
            self.slot_data.remove(i);
        }
    }

    /// Returns the given `LiveNodeId`'s `LiveNode`.
    fn live_node(&self, live_node_id: LiveNodeId) -> LiveResult<&LiveNode> {
        self.live_nodes
            .iter()
            .find(|live_node| live_node.live_node_id == live_node_id)
            .ok_or(LiveError::InvalidLiveNodeId(live_node_id))
    }

    /// Returns true if all nodes are clean.
    fn done(&self) -> bool {
        self.live_node_ids_not_in_state(NodeState::Clean).is_empty()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::address::{DagId, NodeId};
    use crate::live::node::live_node_type::LiveNodeType;

    #[test]
    fn buffer_rgba_receiver_existing() {
        let mut live_dag = LiveDag::new();

        live_dag.slot_data = vec![SlotData {
            live_slot_address_sideless: LiveSlotAddress {
                live_node_id: LiveNodeId(0),
                slot_id: SlotId(0),
            },
            slot_image: SlotImage::gray_from_value(SizePow2::default(), 0.0)
                .expect("could not create SlotImage"),
        }];

        let mut slot_address_map = BTreeMap::new();
        let slot_address = SlotAddress {
            dag_id: DagId(0),
            node_id: NodeId(0),
            slot_id: SlotId(0),
        };
        let live_slot_address = LiveSlotAddressSide {
            live_node_id: LiveNodeId(0),
            slot_id: SlotId(0),
            side: Side::Output,
        };
        slot_address_map.insert(slot_address.with_side(Side::Output), live_slot_address);
        live_dag.slot_address_map = slot_address_map;

        let (sender, receiver) = mpsc::channel();

        live_dag
            .insert_waiting_sender(Mutex::new(sender), slot_address)
            .expect("could not insert waiting sender");

        let (size, buffer) = receiver
            .recv_timeout(Duration::from_secs(1))
            .expect("could not receive message");
        assert_eq!(size, SizePow2::default());
        assert_eq!(buffer, vec![0, 0, 0, 255]);
    }

    #[test]
    fn buffer_rgba_receiver_waiting() {
        let mut live_dag = LiveDag::new();

        // Set up addresses and IDs.
        let node_address = NodeAddress {
            dag_id: DagId(0),
            node_id: NodeId(0),
        };
        let slot_address = node_address.with_slot_id(SlotId(0));
        let live_node_id = LiveNodeId(0);
        let slot_id = SlotId(0);

        // Set up slot address mapping.
        let mut slot_address_map = BTreeMap::new();
        let live_slot_address = LiveSlotAddressSide {
            live_node_id,
            slot_id,
            side: Side::Output,
        };
        slot_address_map.insert(slot_address.with_side(Side::Output), live_slot_address);
        live_dag.slot_address_map = slot_address_map;

        let (sender, receiver) = mpsc::channel();

        // Ask for the image that does not exist yet.
        live_dag
            .insert_waiting_sender(Mutex::new(sender), slot_address)
            .expect("could not insert waiting sender");

        // Create the message with the image and then send it.
        let slot_image = SlotImage::gray_from_value(SizePow2::default(), 0.0)
            .expect("could not create SlotImage");
        let process_message = FinishedProcessing {
            live_node_id,
            slot_images: vec![(slot_id, slot_image)],
        };
        live_dag
            .sender
            .lock()
            .unwrap()
            .send(process_message)
            .expect("receiver had hung up");

        // Set up some internals that `process_messages()` needs to work, and then run it.
        live_dag.live_nodes = vec![LiveNode {
            creator_address: node_address,
            live_node_id,
            live_node_type: LiveNodeType::OutputRgba,
        }];
        live_dag.node_states = vec![(live_node_id, NodeState::Clean)];
        live_dag.processing_node_ids.insert(live_node_id);
        live_dag.process_messages();

        // Receive the buffer we requested.
        let (size, buffer) = receiver
            .recv_timeout(Duration::from_secs(1))
            .expect("could not receive message");

        // Check expectations.
        assert_eq!(size, SizePow2::default());
        assert_eq!(buffer, vec![0, 0, 0, 255]);
    }
}
