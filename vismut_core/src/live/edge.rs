use crate::live::address::LiveSlotAddress;

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub(crate) struct LiveEdge {
    pub output: LiveSlotAddress,
    pub input: LiveSlotAddress,
}

impl LiveEdge {
    pub fn new(output: LiveSlotAddress, input: LiveSlotAddress) -> Self {
        Self { output, input }
    }
}
