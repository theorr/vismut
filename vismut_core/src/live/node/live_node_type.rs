use crate::address::{Side, SlotId};
use crate::live::node::{grayscale, image, merge_rgba, output_rgba, resize};
use crate::live::node::{split_rgba, ProcessData};
use crate::resize::Resize;
use crate::slot::{Slot, SlotType};
use std::fmt::{Display, Formatter};
use std::path::PathBuf;
use std::sync::Mutex;
use threadpool::ThreadPool;

// todo: add function that returns how many inputs the node expects

/// This is the processable representation of a type of node. As opposed the `NodeType`, this enum
/// is made for processing. This means that this type is made behave more similarly. For instance,
/// output nodes have an output slot that image data can be saved on, they don't have the huge
/// special case of nested dags, as each dag node is flattened into their contents in
/// `Engine.prepare()`, and so on.
#[derive(Debug, PartialEq)]
pub enum LiveNodeType {
    /// Reads the specified image from disk. If the image's size is not a power of two it will be
    /// resized to the closest power of two on each axis.
    Image(PathBuf),
    OutputRgba,
    /// Can be used both to simply output a grayscale buffer with the specified value on
    /// every pixel, and as a "default" value for something else. If it has something
    /// plugged into its input slot, it will use that instead of creating a new buffer.
    Grayscale(f32),
    /// Takes 1 rgba image and splits it into 4 grayscale images.
    SplitRgba,
    /// Takes 4 grayscale images and combines them into one rgba image.
    MergeRgba,
    /// Resize `count` number of grayscale inputs to the size determined by the `resize_policy`,
    /// using the `resize_filter` filter. It defaults to a black buffer if it has no input, so use a
    /// `Self::Grayscale` node if you want to control that value.
    Resize {
        count: usize,
        resize: Resize,
        default: Vec<f32>,
    },
}

impl Display for LiveNodeType {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let text = match self {
            Self::Image(path) => format!("Image({})", path.display()),
            Self::OutputRgba => "OutputRgba".to_string(),
            Self::Grayscale(value) => format!("Grayscale({:.3})", value),
            Self::SplitRgba => "SplitRgba".to_string(),
            Self::MergeRgba => "MergeRgba".to_string(),
            Self::Resize {
                count,
                resize,
                default,
            } => {
                format!(
                    "ResizeCount: {}, Resize: {}, Defaults: {:?} )",
                    count, resize, default
                )
            }
        };

        write!(f, "{}", text)
    }
}

impl LiveNodeType {
    pub(crate) fn slots(&self, side: Side) -> Vec<Slot> {
        if let Side::Input = side {
            match self {
                Self::Image(..) => Vec::new(),
                Self::OutputRgba => vec![Slot::new(SlotType::Rgba, SlotId(0), "input")],
                Self::Grayscale(..) => vec![Slot::new(SlotType::Gray, SlotId(0), "input")],
                Self::SplitRgba => vec![Slot::new(SlotType::Rgba, SlotId(0), "input")],
                Self::MergeRgba => {
                    vec![
                        Slot::new(SlotType::Gray, SlotId(0), "red"),
                        Slot::new(SlotType::Gray, SlotId(1), "green"),
                        Slot::new(SlotType::Gray, SlotId(2), "blue"),
                        Slot::new(SlotType::Gray, SlotId(3), "alpha"),
                    ]
                }
                Self::Resize { count, .. } => {
                    let count = *count;
                    let mut slots = Vec::with_capacity(count);

                    for i in 0..count {
                        let slot = Slot::new(SlotType::Gray, SlotId(i), &i.to_string());
                        slots.push(slot);
                    }

                    slots
                }
            }
        } else {
            match self {
                Self::Image(..) => vec![Slot::new(SlotType::Rgba, SlotId(0), "output")],
                Self::OutputRgba => vec![Slot::new(SlotType::Rgba, SlotId(0), "output")],
                Self::Grayscale(..) => vec![Slot::new(SlotType::Gray, SlotId(0), "output")],
                Self::SplitRgba => {
                    vec![
                        Slot::new(SlotType::Gray, SlotId(0), "red"),
                        Slot::new(SlotType::Gray, SlotId(1), "green"),
                        Slot::new(SlotType::Gray, SlotId(2), "blue"),
                        Slot::new(SlotType::Gray, SlotId(3), "alpha"),
                    ]
                }
                Self::MergeRgba => vec![Slot::new(SlotType::Rgba, SlotId(0), "output")],
                Self::Resize { count, .. } => {
                    let count = *count;
                    let mut slots = Vec::with_capacity(count);

                    for i in 0..count {
                        let slot = Slot::new(SlotType::Gray, SlotId(i), &i.to_string());
                        slots.push(slot);
                    }

                    slots
                }
            }
        }
    }

    pub(crate) fn process(&self, thread_pool: &Mutex<ThreadPool>, process_data: ProcessData) {
        match self {
            Self::Image(path_buf) => image::process(process_data, thread_pool, path_buf.clone()),
            Self::OutputRgba => output_rgba::process(process_data),
            Self::Grayscale(value) => grayscale::process(process_data, *value),
            Self::SplitRgba => split_rgba::process(process_data),
            Self::MergeRgba => merge_rgba::process(process_data),
            Self::Resize {
                count,
                resize,
                default,
            } => resize::process(
                process_data,
                thread_pool,
                *resize,
                Default::default(),
                *count,
                default.clone(),
            ),
        };
    }
}
