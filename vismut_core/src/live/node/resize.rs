use crate::address::SlotId;
use crate::live::node::ProcessData;
use crate::live::slot_data::{Buffer, SlotImage};
use crate::pow_two::{Pow2, Pow2Relative, SizePow2};
use crate::resize::{Resize, ResizeFilter, ResizePolicy};
use image::Luma;
use std::sync::{Arc, Mutex};
use threadpool::ThreadPool;

pub(super) fn process(
    process_data: ProcessData,
    thread_pool: &Mutex<ThreadPool>,
    resize: Resize,
    parent_size: SizePow2,
    count: usize,
    default: Vec<f32>,
) {
    let thread_pool = thread_pool.lock().unwrap();
    thread_pool.execute(move || {
        let mut output = Vec::with_capacity(count);

        let target_size = determine_target_size(&process_data, resize.resize_policy, parent_size);

        let slot_ids = (0..count).map(SlotId).collect::<Vec<_>>();
        let slot_images = process_data.get_slot_images(&slot_ids);

        for (i, slot_image) in slot_images.iter().enumerate() {
            let resized_buffer = if let Some(slot_image) = slot_image {
                let buffer = slot_image.inner_gray();
                let resized_buffer = resize_buffer(buffer, target_size, resize.resize_filter);
                SlotImage::Gray(resized_buffer)
            } else {
                // Use a `LiveNodeType::Grayscale` before this node to control the default value.
                SlotImage::gray_from_value(target_size, default[i])
                    .expect("could not create `SlotImage`")
            };

            output.push((SlotId(i), resized_buffer));
        }

        process_data.send(output).unwrap()
    })
}

fn determine_target_size(
    process_data: &ProcessData,
    resize_policy: ResizePolicy,
    parent_size: SizePow2,
) -> SizePow2 {
    match resize_policy {
        ResizePolicy::RelativeToInput(pow_2_relative) => {
            let slot_image = process_data.slot_images.first();

            let first_input_size = if let Some((_, slot_image)) = slot_image {
                slot_image.size()
            } else {
                SizePow2::default()
            };

            first_input_size.saturating_add(pow_2_relative)
        }
        ResizePolicy::RelativeToParent(pow_2_relative) => {
            parent_size.saturating_add(pow_2_relative)
        }
        ResizePolicy::Absolute(size_pow_2) => size_pow_2,
    }
}

fn resize_buffer(
    buffer: &Arc<Buffer>,
    target_size: SizePow2,
    resize_filter: ResizeFilter,
) -> Arc<Buffer> {
    let (width, height) = (
        Pow2::log_2(buffer.width() as usize).expect("width was not a power of two"),
        Pow2::log_2(buffer.height() as usize).expect("height was not a power of two"),
    );
    let input_size = SizePow2::new(width, height);
    let (width_difference, height_difference) = target_size.difference(input_size);

    match resize_filter {
        ResizeFilter::Nearest => {
            let resized_horizontal = resize_nearest_horizontal(buffer, width_difference);
            resize_nearest_vertical(&resized_horizontal, height_difference)
        }
        ResizeFilter::Bilinear => {
            let resized_horizontal = resize_linear_horizontal(buffer, width_difference);
            resize_linear_vertical(&resized_horizontal, height_difference)
        }
    }
}

fn resize_nearest_horizontal(buffer: &Arc<Buffer>, difference: Pow2Relative) -> Arc<Buffer> {
    let (width, height) = buffer.dimensions();
    let power_difference = difference.inner();
    let difference_abs = 2_u32.pow(power_difference.abs() as u32);

    match power_difference {
        i if i > 0 => {
            let resized_buffer = Buffer::from_fn(width * difference_abs, height, |x, y| {
                let x = x / difference_abs;
                *buffer.get_pixel(x, y)
            });

            Arc::new(resized_buffer)
        }
        i if i < 0 => {
            let resized_buffer = Buffer::from_fn(width / difference_abs, height, |x, y| {
                let x = x * difference_abs;
                *buffer.get_pixel(x, y)
            });

            Arc::new(resized_buffer)
        }
        _ => Arc::clone(buffer),
    }
}

fn resize_nearest_vertical(buffer: &Arc<Buffer>, difference: Pow2Relative) -> Arc<Buffer> {
    let (width, height) = buffer.dimensions();
    let power_difference = difference.inner();
    let difference_abs = 2_u32.pow(power_difference.abs() as u32);

    match power_difference {
        i if i > 0 => {
            let height = height * difference_abs;
            let resized_buffer = Buffer::from_fn(width, height, |x, y| {
                let y = y / difference_abs;
                *buffer.get_pixel(x, y)
            });

            Arc::new(resized_buffer)
        }
        i if i < 0 => {
            let height = height / difference_abs;
            let resized_buffer = Buffer::from_fn(width, height, |x, y| {
                let y = y * difference_abs;
                *buffer.get_pixel(x, y)
            });

            Arc::new(resized_buffer)
        }
        _ => Arc::clone(buffer),
    }
}

fn resize_linear_horizontal(buffer: &Arc<Buffer>, difference: Pow2Relative) -> Arc<Buffer> {
    let (source_width, source_height) = buffer.dimensions();
    let power_difference = difference.inner();
    let difference_abs = 2_u32.pow(power_difference.abs() as u32);

    match power_difference {
        i if i > 0 => {
            let new_width = source_width * difference_abs;
            let resized_buffer = Buffer::from_fn(new_width, source_height, |x, y| {
                let x_source = x / difference_abs;
                let x_source_next = (x_source + 1) % source_width;

                let sample_a = buffer.get_pixel(x_source, y).0[0];
                let sample_b = buffer.get_pixel(x_source_next, y).0[0];

                let interpolation_amount = (x % difference_abs) as f32 / difference_abs as f32;

                let final_value = ((sample_b - sample_a) * interpolation_amount) + sample_a;
                Luma::from([final_value])
            });

            Arc::new(resized_buffer)
        }
        i if i < 0 => {
            let new_width = source_width / difference_abs;
            let resized_buffer = Buffer::from_fn(new_width, source_height, |x, y| {
                let x_source = x * difference_abs;

                let mut result = 0.0_f32;
                for i in 0..difference_abs {
                    result += buffer.get_pixel(x_source + i, y).0[0];
                }
                result /= difference_abs as f32;

                Luma::from([result])
            });

            Arc::new(resized_buffer)
        }
        _ => Arc::clone(buffer),
    }
}

fn resize_linear_vertical(buffer: &Arc<Buffer>, difference: Pow2Relative) -> Arc<Buffer> {
    let (source_width, source_height) = buffer.dimensions();
    let power_difference = difference.inner();
    let difference_abs = 2_u32.pow(power_difference.abs() as u32);

    match power_difference {
        i if i > 0 => {
            let new_height = source_height * difference_abs;
            let resized_buffer = Buffer::from_fn(source_width, new_height, |x, y| {
                let y_source = y / difference_abs;
                let y_source_next = (y_source + 1) % source_height;

                let sample_a = buffer.get_pixel(x, y_source).0[0];
                let sample_b = buffer.get_pixel(x, y_source_next).0[0];

                let interpolation_amount = (y % difference_abs) as f32 / difference_abs as f32;

                let final_value = ((sample_b - sample_a) * interpolation_amount) + sample_a;
                Luma::from([final_value])
            });

            Arc::new(resized_buffer)
        }
        i if i < 0 => {
            let new_height = source_height / difference_abs;
            let resized_buffer = Buffer::from_fn(source_width, new_height, |x, y| {
                let y_source = y * difference_abs;

                let mut result = 0.0_f32;
                for i in 0..difference_abs {
                    result += buffer.get_pixel(x, y_source + i).0[0];
                }
                result /= difference_abs as f32;

                Luma::from([result])
            });

            Arc::new(resized_buffer)
        }
        _ => Arc::clone(buffer),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::live::address::LiveNodeId;
    use crate::live::node::FinishedProcessing;
    use crate::pow_two::MAX_POW;
    use std::sync::mpsc;

    // Nearest neighbor filtering.
    #[test]
    fn nearest_horizontal_upscale() {
        let buffer = Buffer::from_raw(
            2,
            2,
            vec![
                1.0, 2.0, //
                3.0, 4.0,
            ],
        )
        .expect("could not create buffer");
        let buffer = Arc::new(buffer);
        let difference = Pow2Relative::new(2);

        let buffer = resize_nearest_horizontal(&buffer, difference);

        assert_eq!(
            buffer.as_raw(),
            &vec![
                1.0, 1.0, 1.0, 1.0, 2.0, 2.0, 2.0, 2.0, //
                3.0, 3.0, 3.0, 3.0, 4.0, 4.0, 4.0, 4.0,
            ],
            "buffer did not match expectations"
        );
    }

    #[test]
    fn nearest_horizontal_downscale() {
        let buffer = Buffer::from_raw(
            8,
            2,
            vec![
                1.0, 0.0, 0.0, 0.0, 2.0, 0.0, 0.0, 0.0, //
                3.0, 0.0, 0.0, 0.0, 4.0, 0.0, 0.0, 0.0,
            ],
        )
        .expect("could not create buffer");
        let buffer = Arc::new(buffer);
        let difference = Pow2Relative::new(-2);

        let buffer = resize_nearest_horizontal(&buffer, difference);

        assert_eq!(
            buffer.as_raw(),
            &vec![
                1.0, 2.0, //
                3.0, 4.0,
            ],
            "buffer did not match expectations"
        );
    }

    #[test]
    fn nearest_vertical_upscale() {
        let buffer = Buffer::from_raw(
            2,
            2,
            vec![
                1.0, 2.0, //
                3.0, 4.0,
            ],
        )
        .expect("could not create buffer");
        let buffer = Arc::new(buffer);
        let difference = Pow2Relative::new(2);

        let buffer = resize_nearest_vertical(&buffer, difference);

        assert_eq!(
            buffer.as_raw(),
            &vec![
                1.0, 2.0, //
                1.0, 2.0, //
                1.0, 2.0, //
                1.0, 2.0, //
                3.0, 4.0, //
                3.0, 4.0, //
                3.0, 4.0, //
                3.0, 4.0,
            ],
            "buffer did not match expectations"
        );
    }

    #[test]
    fn nearest_vertical_downscale() {
        let buffer = Buffer::from_raw(
            2,
            8,
            vec![
                1.0, 2.0, //
                0.0, 0.0, //
                0.0, 0.0, //
                0.0, 0.0, //
                3.0, 4.0, //
                0.0, 0.0, //
                0.0, 0.0, //
                0.0, 0.0,
            ],
        )
        .expect("could not create buffer");
        let buffer = Arc::new(buffer);
        let difference = Pow2Relative::new(-2);

        let buffer = resize_nearest_vertical(&buffer, difference);

        assert_eq!(
            buffer.as_raw(),
            &vec![
                1.0, 2.0, //
                3.0, 4.0,
            ],
            "buffer did not match expectations"
        );
    }

    // Linear filtering
    #[test]
    fn linear_horizontal_upscale() {
        let buffer = Buffer::from_raw(
            2,
            2,
            vec![
                1.0, 2.0, //
                3.0, 4.0,
            ],
        )
        .expect("could not create buffer");
        let buffer = Arc::new(buffer);
        let difference = Pow2Relative::new(2);

        let buffer = resize_linear_horizontal(&buffer, difference);

        // Multiply by 4 and round to ints to simplify comparisons.
        let buffer_int = buffer
            .pixels()
            .map(|pixel| ((pixel.0[0] * 4.0) + 0.5) as usize)
            .collect::<Vec<_>>();

        assert_eq!(
            buffer_int,
            vec![
                4, 5, 6, 7, 8, 7, 6, 5, //
                12, 13, 14, 15, 16, 15, 14, 13,
            ],
            "buffer did not match expectations"
        );
    }

    #[test]
    fn linear_horizontal_downscale() {
        let buffer = Buffer::from_raw(
            8,
            2,
            vec![
                1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, //
                2.0, 1.0, 1.0, 0.0, 4.0, 0.0, 4.0, 0.0,
            ],
        )
        .expect("could not create buffer");
        let buffer = Arc::new(buffer);
        let difference = Pow2Relative::new(-2);

        let buffer = resize_linear_horizontal(&buffer, difference);

        // Multiply by 4 and round to ints to simplify comparisons.
        let buffer_int = buffer
            .pixels()
            .map(|pixel| ((pixel.0[0] * 4.0) + 0.5) as usize)
            .collect::<Vec<_>>();

        assert_eq!(
            buffer_int,
            vec![
                4, 2, //
                4, 8,
            ],
            "buffer did not match expectations"
        );
    }

    #[test]
    fn linear_vertical_upscale() {
        let buffer = Buffer::from_raw(
            2,
            2,
            vec![
                1.0, 2.0, //
                3.0, 4.0,
            ],
        )
        .expect("could not create buffer");
        let buffer = Arc::new(buffer);
        let difference = Pow2Relative::new(2);

        let buffer = resize_linear_vertical(&buffer, difference);

        // Multiply by 4 and round to ints to simplify comparisons.
        let buffer_int = buffer
            .pixels()
            .map(|pixel| ((pixel.0[0] * 4.0) + 0.5) as usize)
            .collect::<Vec<_>>();

        assert_eq!(
            buffer_int,
            vec![
                4, 8, //
                6, 10, //
                8, 12, //
                10, 14, //
                12, 16, //
                10, 14, //
                8, 12, //
                6, 10,
            ],
            "buffer did not match expectations"
        );
    }

    #[test]
    fn linear_vertical_downscale() {
        let buffer = Buffer::from_raw(
            2,
            8,
            vec![
                1.0, 1.0, //
                1.0, 1.0, //
                1.0, 1.0, //
                0.0, 0.0, //
                2.0, 1.0, //
                1.0, 0.0, //
                4.0, 0.0, //
                4.0, 0.0,
            ],
        )
        .expect("could not create buffer");
        let buffer = Arc::new(buffer);
        let difference = Pow2Relative::new(-2);

        let buffer = resize_linear_vertical(&buffer, difference);

        // Multiply by 4 and round to ints to simplify comparisons.
        let buffer_int = buffer
            .pixels()
            .map(|pixel| ((pixel.0[0] * 4.0) + 0.5) as usize)
            .collect::<Vec<_>>();

        assert_eq!(
            buffer_int,
            vec![
                3, 3, //
                11, 1,
            ],
            "buffer did not match expectations"
        );
    }

    #[test]
    fn determine_target_size() {
        let (sender, _) = mpsc::channel();
        let slot_image = SlotImage::gray_from_value(SizePow2::new(Pow2::new(2), Pow2::new(4)), 0.0)
            .expect("could not create `SlotImage`");
        let process_data = ProcessData {
            sender,
            live_node_id: LiveNodeId(0),
            slot_images: vec![(SlotId(0), slot_image)],
        };
        let parent_size = SizePow2::default();

        let resize_policy = ResizePolicy::RelativeToInput(Pow2Relative::new(2));
        let target_size = super::determine_target_size(&process_data, resize_policy, parent_size);
        let compare_size = SizePow2::new(Pow2::new(4), Pow2::new(6));
        assert_eq!(
            target_size, compare_size,
            "determined size did not match expectation"
        );

        let resize_policy = ResizePolicy::RelativeToInput(Pow2Relative::new(-10));
        let target_size = super::determine_target_size(&process_data, resize_policy, parent_size);
        let compare_size = SizePow2::new(Pow2::new(0), Pow2::new(2));
        assert_eq!(
            target_size, compare_size,
            "determined size did not match expectation"
        );

        let resize_policy = ResizePolicy::RelativeToInput(Pow2Relative::new(MAX_POW as i8));
        let target_size = super::determine_target_size(&process_data, resize_policy, parent_size);
        let compare_size = SizePow2::new(Pow2::new(MAX_POW - 2), Pow2::new(MAX_POW));
        assert_eq!(
            target_size, compare_size,
            "determined size did not match expectation"
        );

        let absolute_size = SizePow2::new(Pow2::new(8), Pow2::new(10));
        let resize_policy = ResizePolicy::Absolute(absolute_size);
        let target_size = super::determine_target_size(&process_data, resize_policy, parent_size);
        assert_eq!(
            target_size, absolute_size,
            "determined size did not match expectation"
        );

        let parent_size = SizePow2::new(Pow2::new(7), Pow2::new(10));
        let resize_policy = ResizePolicy::RelativeToParent(Pow2Relative::new(-1));
        let target_size = super::determine_target_size(&process_data, resize_policy, parent_size);
        let compare_size = SizePow2::new(Pow2::new(6), Pow2::new(9));
        assert_eq!(
            target_size, compare_size,
            "determined size did not match expectation"
        );
    }

    #[test]
    fn process() {
        let (sender, receiver) = mpsc::channel();
        let expected_value = 0.0;
        let slot_image =
            SlotImage::gray_from_value(SizePow2::new(Pow2::new(2), Pow2::new(4)), expected_value)
                .expect("could not create `SlotImage`");
        let live_node_id = LiveNodeId(0);
        let process_data = ProcessData {
            sender,
            live_node_id,
            slot_images: vec![(SlotId(0), slot_image)],
        };

        let thread_pool = Mutex::new(ThreadPool::new(1));

        let expected_size = SizePow2::new(Pow2::new(1), Pow2::new(1));
        let resize = Resize::new(ResizePolicy::Absolute(expected_size), Default::default());

        super::process(
            process_data,
            &thread_pool,
            resize,
            Default::default(),
            1,
            Vec::new(),
        );

        let message = receiver.recv().expect("could not receive a message");

        let expected_slot_image = SlotImage::gray_from_value(expected_size, expected_value)
            .expect("could not create expected image");
        let expected_message = FinishedProcessing {
            live_node_id,
            slot_images: vec![(SlotId(0), expected_slot_image)],
        };

        assert_eq!(message, expected_message);
    }
}
