use crate::address::SlotId;
use crate::live::node::ProcessData;
use crate::live::slot_data::SlotImage;

pub(super) fn process(process_data: ProcessData) {
    let slot_images = process_data.get_slot_images(&[SlotId(0), SlotId(1), SlotId(2), SlotId(3)]);

    let output = vec![(
        SlotId(0),
        SlotImage::Rgba([
            slot_images[0]
                .expect("should always have all inputs")
                .inner_gray()
                .clone(),
            slot_images[1]
                .expect("should always have all inputs")
                .inner_gray()
                .clone(),
            slot_images[2]
                .expect("should always have all inputs")
                .inner_gray()
                .clone(),
            slot_images[3]
                .expect("should always have all inputs")
                .inner_gray()
                .clone(),
        ]),
    )];

    process_data.send(output).unwrap();
}
