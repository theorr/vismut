mod grayscale;
mod image;
pub mod live_node_type;
mod merge_rgba;
mod output_rgba;
mod resize;
mod split_rgba;

use crate::address::{NodeAddress, SlotId};
use crate::engine::VismutPixel;
use crate::error::{Result, VismutError};
use crate::live::address::LiveNodeId;
use crate::live::node::live_node_type::LiveNodeType;
use crate::live::slot_data::SlotImage;
use crate::prelude::Buffer;
use std::sync::mpsc;

#[derive(Debug)]
pub(crate) struct ProcessData {
    pub sender: mpsc::Sender<FinishedProcessing>,
    pub live_node_id: LiveNodeId,
    pub slot_images: Vec<(SlotId, SlotImage)>,
}

impl ProcessData {
    pub fn send(self, slot_images: Vec<(SlotId, SlotImage)>) -> Result<()> {
        self.sender
            .send(FinishedProcessing {
                live_node_id: self.live_node_id,
                slot_images,
            })
            .map_err(|_| VismutError::SendError)
    }

    /// Takes a list of `SlotId`s and returns a list of their corresponding `SlotImage`s. It returns
    /// `None` for each `SlotImage` that does not exist.
    pub fn get_slot_images(&self, slot_ids: &[SlotId]) -> Vec<Option<&SlotImage>> {
        let mut output = Vec::with_capacity(slot_ids.len());

        for slot_id in slot_ids {
            let slot_image = self
                .slot_images
                .iter()
                .find(|(slot_id_cmp, _)| slot_id_cmp == slot_id)
                .map(|(_, slot_image)| slot_image);
            output.push(slot_image);
        }

        output
    }
}

#[derive(Debug, PartialEq)]
pub(crate) struct FinishedProcessing {
    pub live_node_id: LiveNodeId,
    pub slot_images: Vec<(SlotId, SlotImage)>,
}

#[derive(Debug, PartialEq)]
pub(crate) struct LiveNode {
    pub creator_address: NodeAddress,
    pub live_node_id: LiveNodeId,
    pub live_node_type: LiveNodeType,
}

fn pixel_buffer(value: VismutPixel) -> Buffer {
    Buffer::from_raw(1, 1, vec![value]).unwrap()
}
