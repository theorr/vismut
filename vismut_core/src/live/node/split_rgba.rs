use crate::address::SlotId;
use crate::live::node::ProcessData;
use crate::live::slot_data::SlotImage;
use crate::pow_two::SizePow2;

pub(super) fn process(process_data: ProcessData) {
    let slot_image = process_data.get_slot_images(&[SlotId(0)])[0];

    let output = if let Some(slot_image) = slot_image {
        if let SlotImage::Rgba(buffers) = slot_image {
            vec![
                (SlotId(0), SlotImage::Gray(buffers[0].clone())),
                (SlotId(1), SlotImage::Gray(buffers[1].clone())),
                (SlotId(2), SlotImage::Gray(buffers[2].clone())),
                (SlotId(3), SlotImage::Gray(buffers[3].clone())),
            ]
        } else {
            panic!("should not be able to connect anything other than a SlotImage::Rgba here");
        }
    } else {
        vec![
            (
                SlotId(0),
                SlotImage::gray_from_value(SizePow2::default(), 0.0).unwrap(),
            ),
            (
                SlotId(1),
                SlotImage::gray_from_value(SizePow2::default(), 0.0).unwrap(),
            ),
            (
                SlotId(2),
                SlotImage::gray_from_value(SizePow2::default(), 0.0).unwrap(),
            ),
            (
                SlotId(3),
                SlotImage::gray_from_value(SizePow2::default(), 1.0).unwrap(),
            ),
        ]
    };

    process_data.send(output).unwrap();
}
