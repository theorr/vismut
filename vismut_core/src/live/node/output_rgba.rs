use crate::address::SlotId;
use crate::live::node::ProcessData;
use crate::live::slot_data::SlotImage;
use crate::pow_two::SizePow2;

pub(super) fn process(process_data: ProcessData) {
    let slot_image = process_data.get_slot_images(&[SlotId(0)])[0];

    let slot_image = if let Some(slot_image) = slot_image {
        slot_image.clone()
    } else {
        SlotImage::rgba_from_values(SizePow2::default(), [0.0, 0.0, 0.0, 1.0])
            .expect("could not create SlotImage")
    };

    let slot_image = vec![(SlotId(0), slot_image)];
    process_data.send(slot_image).unwrap();
}
