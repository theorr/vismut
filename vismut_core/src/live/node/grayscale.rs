use super::pixel_buffer;
use crate::address::SlotId;
use crate::live::node::ProcessData;
use crate::live::slot_data::SlotImage;
use std::sync::Arc;

pub(super) fn process(process_data: ProcessData, value: f32) {
    let slot_image = process_data.get_slot_images(&[SlotId(0)])[0];

    let output = if let Some(slot_image) = slot_image {
        slot_image.clone()
    } else {
        SlotImage::Gray(Arc::new(pixel_buffer(value)))
    };

    process_data.send(vec![(SlotId(0), output)]).unwrap();
}
