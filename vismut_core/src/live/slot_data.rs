use crate::engine::VismutPixel;
use crate::live::address::LiveSlotAddress;
use crate::pow_two::{Pow2, SizePow2};
use image::{ImageBuffer, Luma};
use std::fmt::{Debug, Formatter};
use std::sync::Arc;

// Todo: Remove this struct and use a BTreeMap<SlotAddressSideless, SlotImage> instead.
#[derive(Debug, PartialEq)]
pub(crate) struct SlotData {
    pub live_slot_address_sideless: LiveSlotAddress,
    pub slot_image: SlotImage,
}

#[derive(Clone, PartialEq)]
pub(crate) enum SlotImage {
    Gray(Arc<Buffer>),
    Rgba([Arc<Buffer>; 4]),
}

impl Debug for SlotImage {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let text = if let Self::Gray(_) = self {
            "Gray"
        } else {
            "Rgba"
        };

        write!(f, "{}", text)
    }
}

impl SlotImage {
    pub fn inner_gray(&self) -> &Arc<Buffer> {
        if let Self::Gray(buffer) = self {
            buffer
        } else {
            panic!("tried getting grayscale buffer from rgba `SlotImage`");
        }
    }

    /// Returns a `SlotImage::Gray` of the given `SizePow2` and each pixel a value of
    /// `VismutPixel`.
    pub fn gray_from_value(size_pow_2: SizePow2, value: VismutPixel) -> Option<Self> {
        let (width, height) = size_pow_2.result();
        let new_buffer = Arc::new(Buffer::from_raw(
            width as u32,
            height as u32,
            vec![value; size_pow_2.pixel_count()],
        )?);

        Some(SlotImage::Gray(new_buffer))
    }

    /// Returns a `SlotImage::Rgba` of the given `SizePow2` and each pixel a value of
    /// `VismutPixel`.
    pub fn rgba_from_values(size_pow_2: SizePow2, value: [VismutPixel; 4]) -> Option<Self> {
        let (width, height) = size_pow_2.result();

        let red = Arc::new(Buffer::from_raw(
            width as u32,
            height as u32,
            vec![value[0]; size_pow_2.pixel_count()],
        )?);
        let green = Arc::new(Buffer::from_raw(
            width as u32,
            height as u32,
            vec![value[0]; size_pow_2.pixel_count()],
        )?);
        let blue = Arc::new(Buffer::from_raw(
            width as u32,
            height as u32,
            vec![value[0]; size_pow_2.pixel_count()],
        )?);
        let alpha = Arc::new(Buffer::from_raw(
            width as u32,
            height as u32,
            vec![value[0]; size_pow_2.pixel_count()],
        )?);

        Some(SlotImage::Rgba([red, green, blue, alpha]))
    }

    pub fn size(&self) -> SizePow2 {
        let buffer = match self {
            Self::Gray(buffer) => buffer,
            Self::Rgba(buffers) => &buffers[0],
        };

        let (width, height) = {
            let (width, height) = (buffer.width() as usize, buffer.height() as usize);
            let (width, height) = (Pow2::log_2(width), Pow2::log_2(height));
            (
                width.unwrap_or_else(|| panic!("width of buffer is not a power of two")),
                height.unwrap_or_else(|| panic!("height of buffer is not a power of two")),
            )
        };

        SizePow2::new(width, height)
    }

    pub fn to_u8(&self) -> Vec<u8> {
        match self {
            Self::Gray(buffer) => buffer
                .pixels()
                .flat_map(|x| {
                    let value = Self::f32_to_u8(x[0]);
                    vec![value, value, value, 255]
                })
                .collect(),
            Self::Rgba(buffers) => buffers[0]
                .pixels()
                .zip(buffers[1].pixels())
                .zip(buffers[2].pixels())
                .zip(buffers[3].pixels())
                .flat_map(|(((r, g), b), a)| vec![r, g, b, a].into_iter())
                .map(|x| Self::f32_to_u8(x[0]))
                .collect(),
        }
    }

    #[inline]
    fn f32_to_u8(value: f32) -> u8 {
        ((value.clamp(0.0, 1.0) * 255.).min(255.)) as u8
    }
}

pub type Buffer = ImageBuffer<Luma<VismutPixel>, Vec<VismutPixel>>;
