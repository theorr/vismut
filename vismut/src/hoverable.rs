// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use bevy::prelude::*;

use crate::{workspace::Workspace, AmbiguitySet, CustomStage, Dragged, ToolState};

#[derive(Component, Deref, DerefMut, Default)]
pub struct CustomHitSize(pub Vec2);

#[derive(Component, Default)]
pub(crate) struct Hoverable;

#[derive(Component, Default)]
pub(crate) struct Hovered;

pub(crate) struct HoverablePlugin;

impl Plugin for HoverablePlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set_to_stage(
            CoreStage::Update,
            SystemSet::new()
                .label(CustomStage::Update)
                .after(CustomStage::Setup)
                // Other
                .with_system(
                    hoverable
                        .with_run_criteria(State::on_update(ToolState::None))
                        .in_ambiguity_set(AmbiguitySet),
                ),
        );
    }
}

fn hoverable(
    mut commands: Commands,
    workspace: Res<Workspace>,
    q_hoverable: Query<
        (Entity, &GlobalTransform, &Sprite, Option<&CustomHitSize>),
        (With<Hoverable>, Without<Dragged>),
    >,
) {
    if workspace.cursor_moved {
        for (entity, global_transform, sprite, custom_hit_size) in q_hoverable.iter() {
            let size = if let Some(custom_hit_size) = custom_hit_size {
                custom_hit_size.0
            } else if let Some(size) = sprite.custom_size {
                size
            } else {
                continue;
            };

            if box_contains_point(
                global_transform.translation.truncate(),
                size,
                workspace.cursor_world,
            ) {
                commands.entity(entity).insert(Hovered);
            } else {
                commands.entity(entity).remove::<Hovered>();
            }
        }
    }
}

pub(crate) fn box_contains_point(box_pos: Vec2, box_size: Vec2, point: Vec2) -> bool {
    let half_size = box_size / 2.;

    box_pos.x - half_size.x < point.x
        && box_pos.x + half_size.x > point.x
        && box_pos.y - half_size.y < point.y
        && box_pos.y + half_size.y > point.y
}
