// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use super::SavedProperties;
use crate::core_translation::TranslateName;
use crate::undo::prelude::*;
use bevy_egui::egui::Ui;
use vismut_core::prelude::*;

pub fn display(
    ui: &mut Ui,
    undo_command_manager: &mut UndoCommandManager,
    node_address: NodeAddress,
    node_type: &NodeType,
    saved_properties: &mut SavedProperties,
) {
    let name_current = if let NodeType::OutputRgba(name) = node_type {
        name
    } else {
        return;
    };

    ui.label("Name");
    if ui
        .text_edit_singleline(&mut saved_properties.name)
        .lost_focus()
    {
        let from = TranslateName(name_current.clone());
        let to = TranslateName(saved_properties.name.clone());

        undo_command_manager.push(Box::new(GuiUndoCommand::new(node_address, from, to)));
        undo_command_manager.push(Box::new(Checkpoint));
    }

    ui.end_row();
}
