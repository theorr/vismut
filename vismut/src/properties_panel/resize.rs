// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::sync_graph::Name;
use crate::undo::gui::GuiUndoCommand;
use crate::undo::prelude::{Checkpoint, UndoCommandManager};
use bevy_egui::egui;
use bevy_egui::egui::Ui;
use std::mem;
use vismut_core::address::NodeAddress;
use vismut_core::pow_two::{Pow2, Pow2Relative, SizePow2};
use vismut_core::prelude::{NodeType, Resize, ResizePolicy};
use vismut_core::resize::ResizeFilter;

pub fn display(
    ui: &mut Ui,
    undo_command_manager: &mut UndoCommandManager,
    node_address: NodeAddress,
    node_type: &NodeType,
) {
    let resize = if let NodeType::MergeRgba(resize) | NodeType::SplitRgba(resize) = node_type {
        resize
    } else {
        return;
    };

    resize_policy_outer(ui, undo_command_manager, node_address, resize);
    resize_policy_inner(ui, undo_command_manager, node_address, resize);
    resize_filter(ui, undo_command_manager, node_address, resize);
}

fn resize_policy_outer(
    ui: &mut Ui,
    undo_command_manager: &mut UndoCommandManager,
    node_address: NodeAddress,
    resize: &Resize,
) {
    let resize_policy_current = &resize.resize_policy;
    let resize_policy_discriminant = mem::discriminant(resize_policy_current);
    let mut resize_policy_discriminant_new = resize_policy_discriminant;

    let input = ResizePolicy::RelativeToInput(Pow2Relative::default());
    let input_discriminant = mem::discriminant(&input);

    let parent = ResizePolicy::RelativeToParent(Pow2Relative::default());
    let parent_discriminant = mem::discriminant(&parent);

    let absolute = ResizePolicy::Absolute(SizePow2::default());
    let absolute_discriminant = mem::discriminant(&absolute);

    ui.label("Size From");
    ui.scope(|ui| {
        ui.selectable_value(
            &mut resize_policy_discriminant_new,
            input_discriminant,
            "Input",
        );
        // ui.selectable_value(
        //     &mut resize_policy_discriminant_new,
        //     parent_discriminant,
        //     "Parent",
        // );
        ui.selectable_value(
            &mut resize_policy_discriminant_new,
            absolute_discriminant,
            "Absolute",
        );
    });

    if resize_policy_discriminant != resize_policy_discriminant_new {
        let resize_policy_new = if resize_policy_discriminant_new == input_discriminant {
            input
        } else if resize_policy_discriminant_new == parent_discriminant {
            parent
        } else {
            absolute
        };

        let resize_new = Resize::new(resize_policy_new, resize.resize_filter);
        undo_command_manager.push(Box::new(GuiUndoCommand::new(
            node_address,
            *resize,
            resize_new,
        )));
        undo_command_manager.push(Box::new(Checkpoint));
    }

    ui.end_row();
}

fn resize_policy_inner(
    ui: &mut Ui,
    undo_command_manager: &mut UndoCommandManager,
    node_address: NodeAddress,
    resize: &Resize,
) {
    match resize.resize_policy {
        ResizePolicy::RelativeToInput(pow_2_relative)
        | ResizePolicy::RelativeToParent(pow_2_relative) => {
            ui.label("Difference");
            let value_current = pow_2_relative.inner();
            let mut value_new = value_current;
            let response = ui.add(egui::Slider::new(&mut value_new, -10..=10).clamp_to_range(true));

            if (response.drag_released() || response.lost_focus()) && value_new != value_current {
                let resize_policy = if let ResizePolicy::RelativeToInput(_) = resize.resize_policy {
                    ResizePolicy::RelativeToInput(Pow2Relative::new(value_new))
                } else {
                    ResizePolicy::RelativeToParent(Pow2Relative::new(value_new))
                };
                let resize_new = Resize::new(resize_policy, resize.resize_filter);

                undo_command_manager.push(Box::new(GuiUndoCommand::new(
                    node_address,
                    *resize,
                    resize_new,
                )));
                undo_command_manager.push(Box::new(Checkpoint));
            }
        }
        ResizePolicy::Absolute(size_pow_2) => {
            let (width_current, height_current) = size_pow_2.inner();
            let mut width_new = width_current;
            let mut height_new = height_current;

            ui.label("Width");
            let response_width =
                ui.add(egui::Slider::new(&mut width_new, 0..=10).clamp_to_range(true));
            ui.end_row();

            ui.label("Height");
            let response_height =
                ui.add(egui::Slider::new(&mut height_new, 0..=10).clamp_to_range(true));

            if (response_width.drag_released()
                || response_width.lost_focus()
                || response_height.drag_released()
                || response_height.lost_focus())
                && (width_new != width_current || height_new != height_current)
            {
                let size_pow_2 = SizePow2::new(Pow2::new(width_new), Pow2::new(height_new));
                let resize_policy = ResizePolicy::Absolute(size_pow_2);
                let resize_new = Resize::new(resize_policy, resize.resize_filter);

                undo_command_manager.push(Box::new(GuiUndoCommand::new(
                    node_address,
                    *resize,
                    resize_new,
                )));
                undo_command_manager.push(Box::new(Checkpoint));
            }
        }
    }

    ui.end_row();
}

fn resize_filter(
    ui: &mut Ui,
    undo_command_manager: &mut UndoCommandManager,
    node_address: NodeAddress,
    resize: &Resize,
) {
    let resize_filter_current = resize.resize_filter;
    let mut resize_filter_new = resize_filter_current;

    ui.label("Resize Filter");
    ui.scope(|ui| {
        ui.selectable_value(&mut resize_filter_new, ResizeFilter::Nearest, "Nearest");
        ui.selectable_value(&mut resize_filter_new, ResizeFilter::Bilinear, "Bilinear");
    });

    if resize_filter_current != resize_filter_new {
        let resize_new = Resize::new(resize.resize_policy, resize_filter_new);
        undo_command_manager.push(Box::new(GuiUndoCommand::new(
            node_address,
            *resize,
            resize_new,
        )));
        undo_command_manager.push(Box::new(Checkpoint));
    }

    ui.end_row();
}

impl Name for ResizePolicy {
    fn title(&self) -> String {
        match self {
            Self::RelativeToInput(_) => "Relative to Input",
            Self::RelativeToParent(_) => "Relative to Parent",
            Self::Absolute(_) => "Absolute",
        }
        .into()
    }
}
