// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use bevy::prelude::*;
use bevy_egui::{
    egui::{self, Ui},
    EguiContext,
};
use vismut_core::prelude::*;

use crate::{
    mouse_interaction::active::Active, shared::NodeAddressComponent, sync_graph::Name,
    undo::prelude::UndoCommandManager, CustomStage,
};

mod grayscale;
mod image_path;
mod output_name;
mod resize;

/// Contains stuff that needs to be saved between frames for `bevy_egui` to work. Since it's
/// immediate mode, we need to save some things between frames.
#[derive(Default)]
pub struct SavedProperties {
    pub changed: bool,
    name: String,
}

pub(crate) struct PropertiesPanelPlugin;

impl Plugin for PropertiesPanelPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(SavedProperties::default())
            .add_system_set_to_stage(
                CoreStage::Update,
                SystemSet::new()
                    .after(CustomStage::Setup)
                    .with_system(properties_menu_enter)
                    .with_system(properties_menu.after(properties_menu_enter)),
            );
    }
}

fn properties_menu_enter(
    mut saved_properties: ResMut<SavedProperties>,
    engine: Res<Engine>,
    q_active_new: Query<&NodeAddressComponent, Added<Active>>,
    q_active: Query<&NodeAddressComponent, With<Active>>,
) {
    // When a node with a name is selected, save its name.
    if let Ok(node_address) = q_active_new.get_single() {
        if let Ok(NodeType::OutputRgba(name)) = engine.node_type(**node_address) {
            saved_properties.name = name.clone();
        }
    }

    // And if the name was changed, set the new saved name.
    if saved_properties.changed {
        if let Ok(node_address) = q_active.get_single() {
            if let Ok(NodeType::OutputRgba(name)) = engine.node_type(**node_address) {
                saved_properties.name = name.clone();
            }
        }
        saved_properties.changed = false;
    }
}

fn properties_menu(
    mut egui_context: ResMut<EguiContext>,
    engine: Res<Engine>,
    q_active: Query<&NodeAddressComponent, With<Active>>,
    mut undo_command_manager: ResMut<UndoCommandManager>,
    mut saved_properties: ResMut<SavedProperties>,
) {
    const WIDTH: f32 = 250.0;

    egui::SidePanel::right("properties")
        .resizable(false)
        .show(egui_context.ctx_mut(), |ui| {
            ui.set_max_width(WIDTH);
            ui.set_min_width(WIDTH);
            ui.set_width(WIDTH);
            if let Some(node_address) = q_active.iter().next() {
                if let Ok(node_type) = engine.node_type(**node_address) {
                    properties_gui(
                        &mut *undo_command_manager,
                        ui,
                        &**node_address,
                        node_type,
                        &mut *saved_properties,
                    );
                }
            } else {
                ui.label("Click a node to make it active");
            }
        });
}

fn properties_gui(
    undo_command_manager: &mut UndoCommandManager,
    ui: &mut Ui,
    node_address: &NodeAddress,
    node_type: &NodeType,
    saved_properties: &mut SavedProperties,
) {
    ui.label(node_type.title());
    egui::Grid::new("properties_grid")
        .num_columns(2)
        .striped(true)
        .show(ui, |ui| {
            resize::display(ui, undo_command_manager, *node_address, node_type);

            grayscale::display(ui, undo_command_manager, *node_address, node_type);
            output_name::display(
                ui,
                undo_command_manager,
                *node_address,
                node_type,
                saved_properties,
            );
            image_path::display(ui, node_type);
        });
}
