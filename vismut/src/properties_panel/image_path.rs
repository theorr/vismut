// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use bevy_egui::egui::Ui;
use vismut_core::prelude::*;

pub fn display(ui: &mut Ui, node_type: &NodeType) {
    let path = if let NodeType::Image(path_buf) = node_type {
        path_buf
    } else {
        return;
    };

    ui.label("Path");
    ui.label(format!("{}", path.display()));

    ui.end_row();
}
