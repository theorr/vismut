// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

#![allow(clippy::type_complexity)] // Avoids many warnings about very complex types.
pub mod alpha_checkerboard;
pub mod box_select;
pub mod camera;
pub mod core_translation;
pub mod delete_tool;
pub mod deselect_tool;
pub mod drag_drop;
// This will be re-enabled when drag and drop is supported in Bevy on Windows.
// pub mod drag_drop_import;
pub mod export;
pub mod export_outputs;
pub mod fence;
pub mod grid;
pub mod hotkeys;
pub mod hoverable;
pub mod material;
pub mod menu_bar;
pub mod mouse_interaction;
pub mod none_tool;
pub mod properties_panel;
pub mod scan_code_input;
pub mod shared;
// pub mod stats_window;
pub mod status_bar;
pub mod sync_graph;
// pub mod thumbnail;
mod drag_drop_import;
pub mod retrieve_engine;
mod stats_window;
mod thumbnail;
pub mod undo;
pub mod vismut;
pub mod workspace;

use bevy::prelude::*;
use bevy::window::PresentMode;
use camera::*;
use drag_drop::*;
use hotkeys::*;
use hoverable::*;
use mouse_interaction::select::Selected;
use sync_graph::*;
use vismut::*;
use workspace::*;

pub const FONT: &str = "fonts/Inter-Light.otf";

#[derive(Debug, Hash, PartialEq, Eq, Clone, AmbiguitySetLabel)]
pub(crate) struct AmbiguitySet;

#[derive(Debug, Hash, PartialEq, Eq, Clone, SystemLabel)]
pub(crate) enum CustomStage {
    /// Gathering input.
    Input,
    Setup,
    Update,
    /// Applying all changes.
    Apply,
}

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
pub(crate) enum GrabToolType {
    Node,
    Slot,
}

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
pub(crate) enum ToolState {
    BoxSelect,
    Export,
    /// If `true`, do "export as".
    ExportOutputs(bool),
    Grab(GrabToolType),
    None,
    Redo,
    Undo,
}

impl Default for ToolState {
    fn default() -> Self {
        Self::None
    }
}

fn main() {
    App::new()
        .insert_resource(WindowDescriptor {
            title: "Vismut".to_string(),
            width: 1024.0,
            height: 768.0,
            present_mode: PresentMode::Fifo,
            ..default()
        })
        .insert_resource(Msaa { samples: 1 })
        .insert_resource(ClearColor(Color::rgb(0.08, 0.08, 0.08)))
        // .insert_resource(bevy::ecs::schedule::ReportExecutionOrderAmbiguities)
        .add_plugins(DefaultPlugins)
        .add_plugin(VismutPlugin)
        .run();
}
