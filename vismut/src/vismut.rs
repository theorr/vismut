// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::ToolState;
use bevy::prelude::*;
use bevy_egui::EguiPlugin;
use std::sync::mpsc;
use std::thread;
use std::time::Duration;
use vismut_core::prelude::*;

#[allow(clippy::large_enum_variant)]
pub enum EngineThreadMessage {
    Engine(Engine),
    ReturnEngine,
}

pub(crate) struct VismutPlugin;

impl Plugin for VismutPlugin {
    fn build(&self, app: &mut App) {
        let (sender_to_thread, receiver_from_main) = mpsc::channel::<EngineThreadMessage>();
        let (sender_to_main, receiver_from_thread) = mpsc::channel::<Engine>();

        thread::spawn(move || loop {
            if let EngineThreadMessage::Engine(mut engine) =
                receiver_from_main.recv().expect("other end hung up")
            {
                loop {
                    engine.run();

                    if let Some(EngineThreadMessage::ReturnEngine) =
                        receiver_from_main.try_iter().last()
                    {
                        sender_to_main.send(engine).expect("other end hung up");
                        break;
                    }

                    // Sleep to avoid using 100% of the thread if nothing needs doing.
                    // todo: Break early if Engine is 100% finished instead.
                    thread::sleep(Duration::from_nanos(1));
                }
            }
        });

        app.insert_resource(Engine::new())
            .insert_non_send_resource(sender_to_thread)
            .insert_non_send_resource(receiver_from_thread)
            .add_state(ToolState::None)
            .add_plugin(crate::retrieve_engine::RetrieveEngine)
            .add_plugin(EguiPlugin)
            .add_plugin(crate::menu_bar::MenuBarPlugin)
            .add_plugin(crate::properties_panel::PropertiesPanelPlugin)
            .add_plugin(crate::status_bar::StatusBarPlugin)
            // .add_plugin(crate::stats_window::StatsWindowPlugin)
            .add_plugin(crate::grid::GridPlugin)
            .add_plugin(crate::fence::FencePlugin)
            .add_plugin(crate::export_outputs::ExportOutputsToolPlugin)
            .add_plugin(crate::scan_code_input::ScanCodeInputPlugin)
            .add_plugin(crate::drag_drop::WorkspaceDragDropPlugin)
            .add_plugin(crate::mouse_interaction::MouseInteractionPlugin)
            .add_plugin(crate::box_select::BoxSelectPlugin)
            .add_plugin(crate::camera::CameraPlugin)
            .add_plugin(crate::workspace::WorkspacePlugin)
            .add_plugin(crate::material::MaterialPlugin)
            .add_plugin(crate::sync_graph::SyncGraphPlugin)
            .add_plugin(crate::deselect_tool::DeselectToolPlugin)
            .add_plugin(crate::hotkeys::HotkeysPlugin)
            .add_plugin(crate::hoverable::HoverablePlugin)
            .add_plugin(crate::thumbnail::ThumbnailPlugin)
            .add_plugin(crate::export::ExportPlugin)
            .add_plugin(crate::none_tool::NoneToolPlugin)
            .add_plugin(crate::undo::undo_command_manager::UndoCommandManagerPlugin)
            .add_plugin(crate::undo::undo_redo_tool::UndoPlugin);
        // This will be re-enabled when drag and drop is supported in Bevy on Windows.
        // .add_plugin(crate::drag_drop_import::DragDropImport);
    }
}
