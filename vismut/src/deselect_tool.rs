// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use bevy::prelude::*;

use crate::{
    scan_code_input::{ScanCode, ScanCodeInput},
    CustomStage, Selected, ToolState,
};

pub(crate) struct DeselectToolPlugin;

impl Plugin for DeselectToolPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_to_stage(
            CoreStage::Update,
            deselect
                .label(CustomStage::Apply)
                .after(CustomStage::Update)
                .with_run_criteria(State::on_update(ToolState::None)),
        );
    }
}

fn deselect(
    input: Res<ScanCodeInput>,
    mut commands: Commands,
    q_selected: Query<Entity, With<Selected>>,
) {
    if input.just_pressed(ScanCode::KeyA) {
        for entity in q_selected.iter() {
            commands.entity(entity).remove::<Selected>();
        }
    }
}
