// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use bevy::prelude::*;
use vismut_core::prelude::*;

use crate::{
    shared::NodeAddressComponent,
    undo::{node::RemoveNode, prelude::*},
    Selected,
};

#[derive(Copy, Clone, Debug)]
pub struct DeleteSelected;
impl UndoCommand for DeleteSelected {
    fn command_type(&self) -> crate::undo::UndoCommandType {
        crate::undo::UndoCommandType::Custom
    }

    fn forward(
        &self,
        world: &mut World,
        undo_command_manager: &mut crate::undo::prelude::UndoCommandManager,
    ) {
        let mut query =
            world.query_filtered::<(&NodeAddressComponent, &Transform), With<Selected>>();
        let engine = world.resource::<Engine>();

        for (node_address, transform) in query.iter(world) {
            let node_type = engine.node_type(node_address.0).unwrap();
            let translation = transform.translation.truncate();

            undo_command_manager
                .commands
                .push_front(Box::new(RemoveNode::new(
                    node_address.0,
                    node_type.clone(),
                    translation,
                )));
        }
    }

    fn backward(&self, _: &mut World, _: &mut crate::undo::prelude::UndoCommandManager) {
        unreachable!("this command is never stored in the undo stack");
    }
}
