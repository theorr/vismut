// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use std::f32::consts::PI;

use bevy::{prelude::*, window::WindowResized};

use crate::{camera::WorkspaceCamera, CustomStage};

pub struct Grid {
    pub width: usize,
    pub height: usize,
    point_size: usize,
}

#[derive(Component, Default)]
struct GridPoint;

#[derive(Component, Default)]
struct GridParent;

#[derive(Bundle, Default)]
struct GridPointBundle {
    grid_point: GridPoint,
    #[bundle]
    sprite_bundle: SpriteBundle,
}

impl Default for Grid {
    fn default() -> Self {
        Self {
            width: 100,
            height: 100,
            point_size: 4,
        }
    }
}

pub struct GridPlugin;
impl Plugin for GridPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(Grid::default())
            .add_startup_system(setup)
            .add_system_set_to_stage(
                CoreStage::Update,
                SystemSet::new()
                    .label(CustomStage::Apply)
                    .with_system(move_grid)
                    .with_system(resize_grid),
            );
    }
}

fn setup(mut commands: Commands) {
    commands
        .spawn()
        .insert(GridParent)
        .insert(Transform::from_translation(Vec3::Z))
        .insert(GlobalTransform::default());
}

fn move_grid(
    windows: Res<Windows>,
    mut q_grid_parent: Query<&mut Transform, (With<GridParent>, Without<WorkspaceCamera>)>,
    grid: Res<Grid>,
    q_camera: Query<&Transform, With<WorkspaceCamera>>,
) {
    let camera_transform = q_camera.single();
    let window = windows.get_primary().unwrap();

    if let Ok(mut transform) = q_grid_parent.get_single_mut() {
        transform.translation.x = camera_transform.translation.x
            - window.width() / 2.0
            - camera_transform.translation.x % grid.width as f32;
        transform.translation.y = camera_transform.translation.y
            - window.height() / 2.0
            - camera_transform.translation.y % grid.height as f32;
    }
}

fn resize_grid(
    mut commands: Commands,
    mut window_resized_events: EventReader<WindowResized>,
    q_grid_parent: Query<Entity, With<GridParent>>,
    mut q_grid_points: Query<(Entity, &mut Transform), With<GridPoint>>,
    grid: Res<Grid>,
) {
    if let Some(window_resized_event) = window_resized_events.iter().last() {
        let grid_parent_entity = q_grid_parent.single();
        let mut grid_points = q_grid_points.iter_mut();

        let x_count = (window_resized_event.width / grid.width as f32) as usize;
        let y_count = (window_resized_event.height / grid.height as f32) as usize;

        for x in 0..x_count {
            for y in 0..y_count + 2 {
                if let Some((_, mut grid_point_transform)) = grid_points.next() {
                    grid_point_transform.translation.x = (x * grid.width) as f32;
                    grid_point_transform.translation.y = (y * grid.width) as f32;
                } else {
                    commands.entity(grid_parent_entity).with_children(|parent| {
                        spawn_point(parent, &*grid, x, y);
                    });
                }
            }
        }

        for (entity, ..) in grid_points {
            commands.entity(entity).despawn_recursive();
        }
    }
}

fn spawn_point(child_builder: &mut ChildBuilder, grid: &Grid, x: usize, y: usize) {
    child_builder.spawn_bundle(GridPointBundle {
        sprite_bundle: SpriteBundle {
            sprite: Sprite {
                color: Color::hsl(0.0, 0.0, 0.2),
                custom_size: Some(Vec2::new(grid.point_size as f32, grid.point_size as f32)),
                ..default()
            },
            transform: Transform {
                translation: Vec3::new((x * grid.width) as f32, (y * grid.height) as f32, -1.0),
                rotation: Quat::from_rotation_z(PI / 4.0),
                ..default()
            },
            ..default()
        },
        ..default()
    });
}
