// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use std::{f32::consts::PI, fmt::Debug};

use crate::{
    alpha_checkerboard::AlphaCheckeboardMaterial,
    hoverable::CustomHitSize,
    material::slot_type_to_color,
    shared::{NodeAddressComponent, NodeStateComponent, SlotTypeComponent},
    // thumbnail::{Thumbnail, ThumbnailState, THUMBNAIL_SIZE},
    Draggable,
    Hoverable,
    Hovered,
    FONT,
};
use bevy::{
    prelude::*,
    sprite::{Material2dPlugin, MaterialMesh2dBundle, Mesh2dHandle},
};
use rand::Rng;
use vismut_core::prelude::*;

const MARGIN: f32 = 9.0;
const MARGIN_TOP: f32 = 29.0;
const RGBA_SLOT_WIDTH: f32 = 10.0;
const RGBA_SLOT_HEIGHT: f32 = 16.0;
pub const THUMBNAIL_SIZE: f32 = 128.;
pub const NODE_WIDTH: f32 = THUMBNAIL_SIZE + MARGIN * 2.0;
pub const NODE_HEIGHT: f32 = THUMBNAIL_SIZE + MARGIN + MARGIN_TOP;
const THUMBNAIL_Y: f32 = -NODE_HEIGHT / 2.0 + THUMBNAIL_SIZE / 2.0 + MARGIN;
const FONT_SIZE: f32 = 22.0;
const TITLE_Y: f32 = NODE_HEIGHT / 2.0 - MARGIN_TOP / 2.0;
const SLOT_X: f32 = NODE_WIDTH / 2.0;
pub const SLOT_DISTANCE_Y: f32 = 32.0;
pub const SLOT_HIT_WIDTH: f32 = 40.0;
pub const SMALLEST_DEPTH_UNIT: f32 = f32::EPSILON * 500.;
const OUTLINE_WIDTH: f32 = 3.0;

#[derive(Component)]
pub struct Thumbnail;

pub trait Name {
    fn title(&self) -> String;
}

impl Name for NodeType {
    fn title(&self) -> String {
        match self {
            Self::Image(..) => "Image",
            Self::OutputRgba(..) => "Output",
            Self::MergeRgba(..) => "Merge",
            Self::SplitRgba(..) => "Split",
            Self::Grayscale(..) => "Grayscale",
        }
        .into()
    }
}

#[derive(Component)]
pub struct NodeFrame;

// I'm saving the start and end variables for when I want to select the edges themselves.
#[derive(Component, Copy, Clone, Debug)]
pub(crate) struct GuiEdge {
    pub start: Vec2,
    pub end: Vec2,
    pub output_slot_address: SlotAddress,
    pub input_slot_address: SlotAddress,
}

impl From<GuiEdge> for Edge {
    fn from(gui_edge: GuiEdge) -> Self {
        Self {
            slot_address_output: gui_edge.output_slot_address,
            slot_address_input: gui_edge.input_slot_address,
        }
    }
}

#[derive(Copy, Clone, Debug, Component)]
pub struct SlotFrame;

#[derive(Component, Deref, DerefMut, Copy, Clone, Debug, Default, PartialEq)]
pub(crate) struct SlotAddressSideComponent(pub SlotAddressSide);

#[derive(Bundle, Default)]
pub(crate) struct GuiNodeBundle {
    #[bundle]
    sprite_bundle: SpriteBundle,
    hoverable: Hoverable,
    hovered: Hovered,
    draggable: Draggable,
    node_address: NodeAddressComponent,
    node_state: NodeStateComponent,
}

#[derive(Bundle, Default)]
pub(crate) struct SlotBundle {
    #[bundle]
    sprite_bundle: SpriteBundle,
    hoverable: Hoverable,
    draggable: Draggable,
    slot_address_side: SlotAddressSideComponent,
    slot_type: SlotTypeComponent,
}

pub(crate) struct SyncGraphPlugin;

impl Plugin for SyncGraphPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugin(Material2dPlugin::<AlphaCheckeboardMaterial>::default())
            .add_startup_system(setup);
    }
}

fn setup(mut commands: Commands, mut engine: ResMut<Engine>) {
    let dag = Dag::new();
    let dag_id = engine.insert(dag);
    commands.insert_resource(dag_id);
}

pub(crate) fn stretch_between(
    sprite: &mut Sprite,
    transform: &mut Transform,
    start: Vec2,
    end: Vec2,
) {
    let midpoint = start - (start - end) / 2.;
    let distance = start.distance(end);
    let rotation = Vec2::X.angle_between(start - end);

    transform.translation = midpoint.extend(9.0);
    transform.rotation = Quat::from_rotation_z(rotation);
    sprite.custom_size = Some(Vec2::new(distance, 4.0));
}

pub fn remove_gui_node(world: &mut World, node_address: NodeAddress) {
    world
        .resource_mut::<Engine>()
        .remove_node(node_address)
        .unwrap();
    let (entity, _) = world
        .query::<(Entity, &NodeAddressComponent)>()
        .iter(world)
        .find(|(_, node_id_cmp)| node_address == node_id_cmp.0)
        .unwrap();
    despawn_with_children_recursive(world, entity);
}

pub fn spawn_gui_node(
    world: &mut World,
    node_type: NodeType,
    node_address: NodeAddress,
    translation: Vec2,
) -> Entity {
    let title = node_type.title();

    let mut meshes = world.remove_resource::<Assets<Mesh>>().unwrap();
    let mut custom_materials = world
        .remove_resource::<Assets<AlphaCheckeboardMaterial>>()
        .unwrap();

    let font = world.resource::<AssetServer>().load(FONT);

    let text_style = TextStyle {
        font,
        font_size: FONT_SIZE,
        color: Color::WHITE,
    };
    let text_alignment = TextAlignment {
        horizontal: HorizontalAlign::Center,
        vertical: VerticalAlign::Center,
    };

    let mesh = meshes.add(Mesh::from(shape::Quad {
        size: Vec2::new(THUMBNAIL_SIZE, THUMBNAIL_SIZE),
        flip: false,
    }));

    let scale = world
        .resource::<Windows>()
        .get_primary()
        .unwrap()
        .scale_factor() as f32;

    let entity = world
        .spawn()
        .insert_bundle(GuiNodeBundle {
            // Base
            sprite_bundle: SpriteBundle {
                sprite: Sprite {
                    color: Color::rgb(0.18, 0.18, 0.18),
                    custom_size: Some(Vec2::new(NODE_WIDTH, NODE_HEIGHT)),
                    ..default()
                },
                transform: Transform::from_translation(Vec3::new(
                    translation.x,
                    translation.y,
                    rand::thread_rng().gen_range(0.0..9.0),
                )),
                ..default()
            },
            node_address: NodeAddressComponent(node_address),
            ..default()
        })
        .with_children(|parent| {
            // Title
            parent.spawn_bundle(Text2dBundle {
                text: Text::with_section(title, text_style, text_alignment),
                transform: Transform::from_translation(Vec3::new(0.0, TITLE_Y, 0.0001)),
                ..default()
            });

            // Thumbnail
            parent
                .spawn_bundle(SpriteBundle {
                    sprite: Sprite {
                        custom_size: Some(Vec2::new(THUMBNAIL_SIZE, THUMBNAIL_SIZE)),
                        ..default()
                    },
                    transform: Transform::from_translation(Vec3::new(
                        0.,
                        THUMBNAIL_Y,
                        SMALLEST_DEPTH_UNIT * 2.0,
                    )),
                    ..default()
                })
                .insert(Thumbnail)
                .with_children(|parent| {
                    // Checkerboard
                    parent.spawn_bundle(MaterialMesh2dBundle {
                        mesh: Mesh2dHandle(mesh),
                        material: custom_materials.add(AlphaCheckeboardMaterial { scale }),
                        transform: Transform::from_translation(-Vec3::Z * SMALLEST_DEPTH_UNIT),
                        ..default()
                    });
                });

            // Slots
            for (i, slot) in node_type.slots(Side::Input).into_iter().enumerate() {
                create_slot(parent, false, i, slot, node_address);
            }

            for (i, slot) in node_type.slots(Side::Output).into_iter().enumerate() {
                create_slot(parent, true, i, slot, node_address);
            }

            // Frame
            parent
                .spawn_bundle(SpriteBundle {
                    sprite: Sprite {
                        custom_size: Some(Vec2::new(
                            NODE_WIDTH + OUTLINE_WIDTH * 2.0,
                            NODE_HEIGHT + OUTLINE_WIDTH * 2.0,
                        )),
                        ..default()
                    },
                    transform: Transform::from_translation(-Vec3::Z * SMALLEST_DEPTH_UNIT),
                    ..default()
                })
                .insert(NodeFrame);
        })
        .id();

    world
        .resource_mut::<Engine>()
        .insert_node_with_address(node_type, node_address)
        .expect("could not insert a node");

    world.insert_resource(meshes);
    world.insert_resource(custom_materials);

    entity
}

fn create_slot(
    parent: &mut WorldChildBuilder,
    output: bool,
    i: usize,
    slot: Slot,
    node_address: NodeAddress,
) {
    let (side, pos_x) = if output {
        (Side::Output, SLOT_X)
    } else {
        (Side::Input, -SLOT_X)
    };

    let translation = Vec3::new(
        pos_x,
        NODE_HEIGHT / 2.0 - MARGIN_TOP - RGBA_SLOT_HEIGHT / 2.0 - SLOT_DISTANCE_Y * i as f32,
        SMALLEST_DEPTH_UNIT * 2.0,
    );

    let (rotation, custom_size) = if let SlotType::Gray = slot.slot_type {
        (
            Quat::from_rotation_z(PI * 0.25),
            Vec2::new(RGBA_SLOT_WIDTH, RGBA_SLOT_WIDTH) + Vec2::splat(2.0),
        )
    } else {
        (
            Quat::default(),
            Vec2::new(RGBA_SLOT_WIDTH, RGBA_SLOT_HEIGHT),
        )
    };

    let color = slot_type_to_color(slot.slot_type, 0.6);

    let hit_size = Vec2::new(SLOT_HIT_WIDTH, SLOT_DISTANCE_Y);

    parent
        .spawn_bundle(SlotBundle {
            sprite_bundle: SpriteBundle {
                sprite: Sprite {
                    color,
                    custom_size: Some(custom_size - Vec2::splat(2.0)),
                    ..default()
                },
                transform: Transform {
                    rotation,
                    translation,
                    ..default()
                },
                ..default()
            },
            slot_address_side: SlotAddressSideComponent(
                node_address.with_slot_id(slot.slot_id).with_side(side),
            ),
            slot_type: SlotTypeComponent(slot.slot_type),
            ..default()
        })
        .insert(CustomHitSize(hit_size))
        .with_children(|parent| {
            parent
                .spawn_bundle(SpriteBundle {
                    sprite: Sprite {
                        color: Color::BLACK,
                        custom_size: Some(custom_size),
                        ..default()
                    },
                    transform: Transform::from_translation(-Vec3::Z * SMALLEST_DEPTH_UNIT),
                    ..default()
                })
                .insert(SlotFrame);
        });
}
