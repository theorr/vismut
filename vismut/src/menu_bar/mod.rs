// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

pub mod add_node;
mod file;

use bevy::{app::AppExit, prelude::*};
use bevy_egui::{
    egui::{self, FontData, FontDefinitions, FontFamily, Layout, Pos2, Ui},
    EguiContext,
};
use vismut_core::prelude::*;

use crate::node::MoveToCursor;
use crate::{
    scan_code_input::{ScanCode, ScanCodeInput},
    undo::prelude::UndoCommandManager,
    workspace::Workspace,
    CustomStage, DragDropMode, ToolState,
};

use self::{
    add_node::{add_node_menu, floating_add_menu},
    file::{file_menu, floating_file_menu},
};

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
pub enum FloatingMenuState {
    None,
    AddNode,
    File,
}

struct FloatingMenuPos(Pos2);
pub(crate) struct MenuBarPlugin;

impl Plugin for MenuBarPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(FloatingMenuPos(Pos2::ZERO))
            .add_startup_system(set_font)
            .add_state(FloatingMenuState::None)
            .add_system_set_to_stage(
                CoreStage::Update,
                SystemSet::new()
                    .after(CustomStage::Setup)
                    .with_system(
                        reset_floating_menu_state
                            .with_run_criteria(State::on_enter(ToolState::None)),
                    )
                    .with_system(menu_bar)
                    .with_system(
                        floating_enter
                            .with_run_criteria(State::on_enter(FloatingMenuState::AddNode)),
                    )
                    .with_system(
                        floating_add_menu
                            .with_run_criteria(State::on_update(FloatingMenuState::AddNode)),
                    )
                    .with_system(
                        floating_enter.with_run_criteria(State::on_enter(FloatingMenuState::File)),
                    )
                    .with_system(
                        floating_file_menu
                            .with_run_criteria(State::on_update(FloatingMenuState::File)),
                    ),
            );
    }
}

fn set_font(mut egui_context: ResMut<EguiContext>) {
    let mut fonts = FontDefinitions::default();

    fonts.font_data.insert(
        "inter".to_owned(),
        FontData::from_static(include_bytes!("../../assets/fonts/Inter-Light.otf")),
    );
    fonts
        .families
        .get_mut(&FontFamily::Proportional)
        .unwrap()
        .insert(0, "inter".to_owned());

    egui_context.ctx_mut().set_fonts(fonts);
}

fn canceled_menu(i_mouse_button: &Input<MouseButton>, scan_code_input: &ScanCodeInput) -> bool {
    i_mouse_button.any_just_released([MouseButton::Left, MouseButton::Right])
        || i_mouse_button.just_pressed(MouseButton::Middle)
        || scan_code_input.just_pressed(ScanCode::Escape)
}

fn reset_floating_menu_state(mut floating_menu_state: ResMut<State<FloatingMenuState>>) {
    let _ = floating_menu_state.overwrite_replace(FloatingMenuState::None);
}

#[allow(clippy::too_many_arguments)]
fn menu_bar(
    mut egui_context: ResMut<EguiContext>,
    mut undo_command_manager: ResMut<UndoCommandManager>,
    mut move_to_cursor: ResMut<MoveToCursor>,
    mut tool_state: ResMut<State<ToolState>>,
    mut engine: ResMut<Engine>,
    dag_id: Res<DagId>,
    mut sc_input: ResMut<ScanCodeInput>,
    mut app_exit_events: EventWriter<AppExit>,
    mut drag_drop_mode: ResMut<DragDropMode>,
    // mut ev_toggle_stats_window: EventWriter<ToggleStatsWindow>,
) {
    egui::Area::new("menu_bar")
        .fixed_pos(Pos2::ZERO)
        .enabled(*tool_state.current() == ToolState::None)
        .show(egui_context.ctx_mut(), |ui| {
            ui.allocate_ui_with_layout(ui.min_size(), Layout::left_to_right(), |ui| {
                ui.menu_button("File (F4)", |ui| {
                    if file_menu(&mut app_exit_events, ui, &mut *tool_state) {
                        ui.close_menu();
                    }
                });
                ui.menu_button("Add Node (Shift A)", |ui| {
                    if add_node_menu(
                        &mut undo_command_manager,
                        &mut move_to_cursor,
                        &mut engine,
                        *dag_id,
                        &mut sc_input,
                        ui,
                        &mut *drag_drop_mode,
                    ) {
                        ui.close_menu();
                    }
                });
                // ui.menu_button("View", |ui| {
                //     if ui.button("Stats").clicked() {
                //         // ev_toggle_stats_window.send(ToggleStatsWindow);
                //         ui.close_menu();
                //     }
                // });
                ui.menu_button("Help", |ui| {
                    if ui.button("🌐 Manual").clicked() {
                        let _ = webbrowser::open(
                            "https://gitlab.com/vismutorg/vismut/-/blob/main/MANUAL.md",
                        );
                        ui.close_menu();
                    };
                    if ui.button("🌐 Community").clicked() {
                        let _ = webbrowser::open("https://vismut.zulipchat.com");
                        ui.close_menu();
                    }
                    if ui.button("🌐 Bug Tracker").clicked() {
                        let _ = webbrowser::open("https://gitlab.com/vismutorg/vismut/-/issues");
                        ui.close_menu();
                    };
                });
            })
        });
}

fn floating_enter(
    workspace: Res<Workspace>,
    windows: Res<Windows>,
    mut floating_menu_pos: ResMut<FloatingMenuPos>,
) {
    let cursor_pos_x = workspace.cursor_screen.x;
    let cursor_pos_y = {
        let window_height = windows.get_primary().unwrap().height();
        (workspace.cursor_screen.y - window_height) * -1.0
    };

    floating_menu_pos.0 = Pos2::new(cursor_pos_x, cursor_pos_y);
}

fn hotkey_button(ui: &mut Ui, sc_input: &mut ScanCodeInput, text: &str, hotkey: ScanCode) -> bool {
    ui.button(format!("{} ({})", text, hotkey)).clicked() || scan_code_pressed(sc_input, hotkey)
}

fn scan_code_pressed(sc_input: &mut ScanCodeInput, scan_code: ScanCode) -> bool {
    for scan_code_pressed in sc_input.get_just_pressed() {
        if *scan_code_pressed == scan_code {
            return true;
        }
    }
    false
}
