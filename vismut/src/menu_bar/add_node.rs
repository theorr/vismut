// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use super::{canceled_menu, hotkey_button, FloatingMenuPos, FloatingMenuState};
use crate::node::MoveToCursor;
use crate::{
    drag_drop::Draggable,
    mouse_interaction::select::{ReplaceSelection, Selected},
    scan_code_input::{ScanCode, ScanCodeInput},
    shared::NodeAddressComponent,
    undo::prelude::UndoCommandManager,
    undo::{node::AddNode, prelude::*},
    DragDropMode, GrabToolType, ToolState,
};
use bevy::prelude::*;
use bevy_egui::{
    egui::{self, Ui},
    EguiContext,
};
use native_dialog::FileDialog;
use vismut_core::prelude::*;

/// Returns true if something was clicked.
pub(super) fn add_node_menu(
    undo_command_manager: &mut UndoCommandManager,
    move_to_cursor: &mut MoveToCursor,
    engine: &mut Engine,
    dag_id: DagId,
    sc_input: &mut ScanCodeInput,
    ui: &mut Ui,
    drag_drop_mode: &mut DragDropMode,
) -> bool {
    let mut clicked = false;

    let node_types = if hotkey_button(ui, &mut *sc_input, "Image…", ScanCode::KeyI) {
        clicked = true;

        let file_dialog = FileDialog::new()
            .add_filter(
                "Image",
                &["bmp", "gif", "jpg", "jpeg", "png", "tga", "tiff"],
            )
            .show_open_multiple_file();

        if let Ok(path_bufs) = file_dialog {
            path_bufs
                .into_iter()
                .map(NodeType::Image)
                .collect::<Vec<NodeType>>()
        } else {
            error!("could not open file dialog");
            Vec::new()
        }
    } else if hotkey_button(ui, &mut *sc_input, "Output", ScanCode::KeyO) {
        clicked = true;
        vec![NodeType::OutputRgba("untitled".into())]
    } else if hotkey_button(ui, &mut *sc_input, "Split", ScanCode::KeyS) {
        clicked = true;
        vec![NodeType::SplitRgba(Resize::default())]
    } else if hotkey_button(ui, &mut *sc_input, "Merge", ScanCode::KeyM) {
        clicked = true;
        vec![NodeType::MergeRgba(Resize::default())]
    } else if hotkey_button(ui, &mut *sc_input, "Grayscale", ScanCode::KeyG) {
        clicked = true;
        vec![NodeType::Grayscale(1.0)]
    } else {
        Vec::new()
    };

    if !node_types.is_empty() {
        create_nodes(
            undo_command_manager,
            move_to_cursor,
            engine,
            dag_id,
            &node_types,
        );
    }

    if clicked {
        *drag_drop_mode = DragDropMode::Pressed;
    }

    clicked
}

#[allow(clippy::too_many_arguments)]
pub(super) fn floating_add_menu(
    mut undo_command_manager: ResMut<UndoCommandManager>,
    mut engine: ResMut<Engine>,
    mut move_to_cursor: ResMut<MoveToCursor>,
    dag_id: Res<DagId>,
    i_mouse_button: Res<Input<MouseButton>>,
    mut sc_input: ResMut<ScanCodeInput>,
    mut egui_context: ResMut<EguiContext>,
    mut floating_menu_state: ResMut<State<FloatingMenuState>>,
    floating_menu_pos: Res<FloatingMenuPos>,
    mut ignore_fast_mouse_release: ResMut<DragDropMode>,
) {
    let mut chose_something = false;

    egui::Area::new("add_menu")
        .fixed_pos(floating_menu_pos.0)
        .show(egui_context.ctx_mut(), |ui| {
            chose_something = add_node_menu(
                &mut *undo_command_manager,
                &mut move_to_cursor,
                &mut *engine,
                *dag_id,
                &mut *sc_input,
                ui,
                &mut *ignore_fast_mouse_release,
            );
        });

    if canceled_menu(&*i_mouse_button, &*sc_input) || chose_something {
        floating_menu_state.set(FloatingMenuState::None).unwrap();
    }
}

fn create_nodes(
    undo_command_manager: &mut UndoCommandManager,
    move_to_cursor: &mut MoveToCursor,
    engine: &mut Engine,
    dag_id: DagId,
    node_types: &[NodeType],
) {
    if !node_types.is_empty() {
        for node_type in node_types {
            let node_address = engine.new_address(dag_id).expect("DAG should always exist");
            undo_command_manager.push(Box::new(AddNode::new(
                node_address,
                node_type.clone(),
                Vec2::ZERO,
            )));
        }

        grab_new_nodes(undo_command_manager, move_to_cursor);
    }
}

#[derive(Copy, Clone, Debug)]
pub struct DragToolUndo;
impl UndoCommand for DragToolUndo {
    fn command_type(&self) -> crate::undo::UndoCommandType {
        crate::undo::UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        let mut tool_state = world.resource_mut::<State<ToolState>>();
        let _ = tool_state.overwrite_replace(ToolState::Grab(GrabToolType::Node));
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this command is not saved on the undo stack");
    }
}

#[derive(Copy, Clone, Debug)]
struct SelectNew;
impl UndoCommand for SelectNew {
    fn command_type(&self) -> crate::undo::UndoCommandType {
        crate::undo::UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut query =
            world.query_filtered::<&NodeAddressComponent, (With<Draggable>, Added<NodeAddressComponent>)>();
        let new_node_addresses = query
            .iter(world)
            .map(|node_address| node_address.0)
            .collect();

        undo_command_manager.push_front(Box::new(ReplaceSelection(new_node_addresses)));
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this command is not saved on the undo stack");
    }
}

/// The sneaky variant is not saved on the undo stack. Can probably be replaced with a command that
/// removes the most recent command from the undo stack.
#[derive(Copy, Clone, Debug)]
struct DeselectSneaky;
impl UndoCommand for DeselectSneaky {
    fn command_type(&self) -> crate::undo::UndoCommandType {
        crate::undo::UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        let mut query = world.query_filtered::<Entity, With<Selected>>();

        for entity in query.iter(world).collect::<Vec<Entity>>() {
            world.entity_mut(entity).remove::<Selected>();
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this command is not saved on the undo stack");
    }
}

/// Grabs any nodes that were just created.
pub fn grab_new_nodes(
    undo_command_manager: &mut UndoCommandManager,
    move_to_cursor: &mut MoveToCursor,
) {
    move_to_cursor.0 = true;
    undo_command_manager.push(Box::new(SelectNew));
    undo_command_manager.push(Box::new(DragToolUndo));
}
