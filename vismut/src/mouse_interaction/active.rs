// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::{
    mouse_interaction::select::SelectNode, shared::NodeAddressComponent, undo::prelude::*,
};
use bevy::prelude::*;
use vismut_core::prelude::*;

#[derive(Component, Default)]
pub(crate) struct Active;

fn make_node_active(world: &mut World, node_address: NodeAddress) {
    let mut q_node_id = world.query_filtered::<(Entity, &NodeAddressComponent), Without<Active>>();

    if let Some((entity, _)) = q_node_id
        .iter(world)
        .find(|(_, node_id_component)| node_id_component.0 == node_address)
    {
        world.entity_mut(entity).insert(Active);
    } else {
        warn!("failed to make a node active");
    }
}

fn make_node_not_active(world: &mut World, node_address: NodeAddress) {
    let mut q_node_id = world.query_filtered::<(Entity, &NodeAddressComponent), With<Active>>();

    if let Some((entity, _)) = q_node_id
        .iter(world)
        .find(|(_, node_id_component)| node_id_component.0 == node_address)
    {
        world.entity_mut(entity).remove::<Active>();
    } else {
        warn!("failed to make a node not active");
    }
}

//
// Making nodes active
//

#[derive(Copy, Clone, Debug)]
struct MakeNodeActiveOnly(pub NodeAddress);
impl UndoCommand for MakeNodeActiveOnly {
    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        make_node_active(world, self.0);
    }

    fn backward(&self, world: &mut World, _: &mut UndoCommandManager) {
        make_node_not_active(world, self.0);
    }
}

#[derive(Copy, Clone, Debug)]
pub struct MakeNodeActive(pub NodeAddress);
impl UndoCommand for MakeNodeActive {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut q_active_node_id = world.query_filtered::<&NodeAddressComponent, With<Active>>();
        assert!(
            q_active_node_id.iter(world).count() < 2,
            "there is more than one active node"
        );

        if let Some(active_node_id) = q_active_node_id.iter(world).next() {
            if active_node_id.0 != self.0 {
                let undo_batch: Vec<BoxUndoCommand> = vec![
                    Box::new(MakeNodeNotActive(active_node_id.0)),
                    Box::new(SelectNode(self.0)),
                    Box::new(MakeNodeActiveOnly(self.0)),
                ];
                undo_command_manager.push_front_vec(undo_batch);
            }
        } else {
            let undo_batch: Vec<BoxUndoCommand> = vec![
                Box::new(SelectNode(self.0)),
                Box::new(MakeNodeActiveOnly(self.0)),
            ];
            undo_command_manager.push_front_vec(undo_batch);
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this command is never put on the undo stack");
    }
}

//
// Making nodes not active
//

#[derive(Copy, Clone, Debug)]
struct MakeNodeNotActiveOnly(pub NodeAddress);
impl UndoCommand for MakeNodeNotActiveOnly {
    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        make_node_not_active(world, self.0);
    }

    fn backward(&self, world: &mut World, _: &mut UndoCommandManager) {
        make_node_active(world, self.0);
    }
}

#[derive(Copy, Clone, Debug)]
pub struct MakeNodeNotActive(pub NodeAddress);
impl UndoCommand for MakeNodeNotActive {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut q_active_node_id = world.query_filtered::<&NodeAddressComponent, With<Active>>();
        assert!(
            q_active_node_id.iter(world).count() < 2,
            "there is more than one active node"
        );

        if let Some(active_node_id) = q_active_node_id.iter(world).next() {
            if active_node_id.0 == self.0 {
                undo_command_manager.push_front(Box::new(MakeNodeNotActiveOnly(self.0)));
            } else {
                warn!("tried making a not active node not active");
            }
        } else {
            warn!("could not find an active node to make not active");
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this command is never put on the undo stack");
    }
}

#[derive(Copy, Clone, Debug)]
pub struct MakeNothingActive;
impl UndoCommand for MakeNothingActive {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut q_active_node_id = world.query_filtered::<&NodeAddressComponent, With<Active>>();
        assert!(
            q_active_node_id.iter(world).count() < 2,
            "there is more than one active node"
        );

        if let Some(node_address) = q_active_node_id.iter(world).next() {
            undo_command_manager.push_front(Box::new(MakeNodeNotActive(node_address.0)));
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this undo command is never put on the undo stack");
    }
}
