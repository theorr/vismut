// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::{
    shared::NodeAddressComponent,
    undo::{prelude::*, undo_command_manager::BoxUndoCommand, UndoCommand, UndoCommandType},
};
use bevy::prelude::*;
use vismut_core::prelude::*;

use super::active::MakeNodeNotActive;

#[derive(Component, Default)]
pub(crate) struct Selected;

fn select_node(world: &mut World, node_address: NodeAddress) {
    let mut q_node_id =
        world.query_filtered::<(Entity, &NodeAddressComponent), Without<Selected>>();

    if let Some((entity, _)) = q_node_id
        .iter(world)
        .find(|(_, node_address_component)| node_address_component.0 == node_address)
    {
        world.entity_mut(entity).insert(Selected);
    } else {
        warn!("failed to select a node");
    }
}

fn deselect_node(world: &mut World, node_address: NodeAddress) {
    let mut q_node_id = world.query_filtered::<(Entity, &NodeAddressComponent), With<Selected>>();

    if let Some((entity, _)) = q_node_id
        .iter(world)
        .find(|(_, node_id_component)| node_id_component.0 == node_address)
    {
        world.entity_mut(entity).remove::<Selected>();
    } else {
        warn!("failed to deselect a node");
    }
}

#[derive(Debug)]
pub struct ReplaceSelection(pub Vec<NodeAddress>);
impl UndoCommand for ReplaceSelection {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut q_node_id = world.query::<(&NodeAddressComponent, Option<&Selected>)>();

        for (node_address, selected) in q_node_id.iter(world) {
            let in_new_selection = self.0.contains(&node_address.0);

            if selected.is_none() && in_new_selection {
                undo_command_manager
                    .commands
                    .push_front(Box::new(SelectNodeOnly(node_address.0)));
            } else if selected.is_some() && !in_new_selection {
                undo_command_manager
                    .commands
                    .push_front(Box::new(DeselectNodeOnly(node_address.0)));
            }
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("command is never put on undo stack");
    }
}

//
// Selecting
//

#[derive(Copy, Clone, Debug)]
struct SelectNodeOnly(NodeAddress);
impl UndoCommand for SelectNodeOnly {
    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        select_node(world, self.0);
    }

    fn backward(&self, world: &mut World, _: &mut UndoCommandManager) {
        deselect_node(world, self.0);
    }
}

#[derive(Copy, Clone, Debug)]
pub struct SelectNode(pub NodeAddress);
impl UndoCommand for SelectNode {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut q_node_id = world.query_filtered::<&NodeAddressComponent, Without<Selected>>();

        if q_node_id.iter(world).any(|node_id| node_id.0 == self.0) {
            undo_command_manager.push_front(Box::new(SelectNodeOnly(self.0)));
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this command is never put on the undo stack");
    }
}

//
// Deselecting
//

#[derive(Copy, Clone, Debug)]
struct DeselectNodeOnly(NodeAddress);
impl UndoCommand for DeselectNodeOnly {
    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        deselect_node(world, self.0);
    }

    fn backward(&self, world: &mut World, _: &mut UndoCommandManager) {
        select_node(world, self.0);
    }
}

#[derive(Copy, Clone, Debug)]
pub struct DeselectNode(pub NodeAddress);
impl UndoCommand for DeselectNode {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut q_node_id = world.query_filtered::<&NodeAddressComponent, With<Selected>>();

        if q_node_id.iter(world).any(|node_id| node_id.0 == self.0) {
            let undo_batch: Vec<BoxUndoCommand> = vec![
                Box::new(MakeNodeNotActive(self.0)),
                Box::new(DeselectNodeOnly(self.0)),
            ];
            undo_command_manager.push_front_vec(undo_batch);
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this command is never put on the undo stack");
    }
}

#[derive(Copy, Clone, Debug)]
pub struct DeselectAll;
impl UndoCommand for DeselectAll {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut q_selected = world.query_filtered::<&NodeAddressComponent, With<Selected>>();

        for node_id in q_selected.iter(world) {
            undo_command_manager
                .commands
                .push_front(Box::new(DeselectNodeOnly(node_id.0)));
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("command is never put on undo stack");
    }
}

#[derive(Copy, Clone, Debug)]
pub struct SelectAll;
impl UndoCommand for SelectAll {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut q_not_selected = world.query_filtered::<&NodeAddressComponent, Without<Selected>>();

        for node_id in q_not_selected.iter(world) {
            undo_command_manager
                .commands
                .push_front(Box::new(SelectNodeOnly(node_id.0)));
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("command is never put on undo stack");
    }
}
