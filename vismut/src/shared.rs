// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use bevy::prelude::*;
use vismut_core::prelude::*;

#[derive(Component, Deref, DerefMut, Default)]
pub struct NodeAddressComponent(pub NodeAddress);

#[derive(Component, Deref, DerefMut, Default)]
pub struct NodeStateComponent(pub NodeState);

#[derive(Component, Deref, DerefMut, Default)]
pub struct SlotTypeComponent(pub SlotType);
