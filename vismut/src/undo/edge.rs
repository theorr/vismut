// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use crate::{
    drag_drop::edge::EdgeColor,
    material::slot_type_to_color,
    sync_graph::{stretch_between, GuiEdge, SlotAddressSideComponent, SMALLEST_DEPTH_UNIT},
};

use super::{prelude::*, undo_command_manager::BoxUndoCommand, AddRemove};
use bevy::prelude::*;
use vismut_core::error::VismutError;
use vismut_core::prelude::*;

impl AddRemove for Edge {
    fn add(&self, world: &mut World) {
        add_edge(world, *self, None);
    }

    fn remove(&self, world: &mut World) {
        let mut engine = world.resource_mut::<Engine>();

        if engine.remove_edge(*self).is_ok() {
            info!("removed edge: {:?}", self);
            remove_gui_edge(world, *self);
        } else {
            error!("Couldn't find the edge to remove: {:?}", &self);
        }
    }
}

/// Adds an edge and its corresponding GUI representation.
fn add_edge(world: &mut World, edge: Edge, start_end: Option<(Vec2, Vec2)>) {
    let mut engine = world.resource_mut::<Engine>();

    if let Ok(_) | Err(VismutError::SlotOccupied(_)) = engine.can_connect(edge) {
        engine
            .connect(edge)
            .expect("could not connect nodes, even though it should have been possible");
        add_gui_edge(world, edge, start_end);
    } else {
        error!("could not add the edge")
    }
}

// fn set_thumbnail_state(world: &mut World, node_id: NodeId, thumbnail_state: ThumbnailState) {
//     let mut q_thumbnail_state = world.query::<(&NodeAddressComponent, &mut ThumbnailState)>();
//     if let Some(mut thumbnail_state_iter) = q_thumbnail_state
//         .iter_mut(world)
//         .find(|(node_id_iter, _)| node_id_iter.0 == node_id)
//         .map(|(_, thumbnail_state)| thumbnail_state)
//     {
//         *thumbnail_state_iter = thumbnail_state;
//     }
// }

#[derive(Clone, Copy, Debug)]
pub struct RemoveGuiEdge(pub(crate) GuiEdge);
impl UndoCommand for RemoveGuiEdge {
    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        let edge: Edge = self.0.into();
        edge.remove(world);
    }

    fn backward(&self, world: &mut World, _: &mut UndoCommandManager) {
        let edge: Edge = self.0.into();
        let start_end = Some((self.0.start, self.0.end));

        add_edge(world, edge, start_end);
    }
}

/// Checks if the connection can be made, and if so creates `UndoCommand`s that disconnect each
/// edge connected to the input slot, and an `UndoCommand` that does the connection.
#[derive(Clone, Copy, Debug)]
pub struct AddEdge(pub Edge);
impl UndoCommand for AddEdge {
    fn command_type(&self) -> super::UndoCommandType {
        super::UndoCommandType::Custom
    }

    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager) {
        let mut engine = world.resource_mut::<Engine>();
        let can_connect = engine.can_connect(self.0);

        if let Ok(..) | Err(VismutError::SlotOccupied(..)) = can_connect {
            let mut undo_commands: Vec<BoxUndoCommand> = Vec::new();

            // Remove any input edges.
            let mut q_gui_edge = world.query::<&GuiEdge>();
            for gui_edge in q_gui_edge
                .iter(world)
                .filter(|edge| edge.input_slot_address == self.0.slot_address_input)
            {
                undo_commands.push(Box::new(RemoveGuiEdge(*gui_edge)));
            }

            // Connect the new edge.
            undo_commands.push(Box::new(AddEdgeOnly(self.0)));

            undo_command_manager
                .commands
                .push_front(Box::new(undo_commands));
        } else {
            warn!("tried creating invalid connection");
        }
    }

    fn backward(&self, _: &mut World, _: &mut UndoCommandManager) {
        unreachable!("this `UndoCommand` is never put on the Undo stack");
    }
}

/// Only creates a connection, without removing any existing connections.
/// You should generally use `AddEdge` instead.
///
/// Fails if an `Edge` is already connected to the same input slot.
#[derive(Clone, Copy, Debug)]
pub struct AddEdgeOnly(pub Edge);
impl UndoCommand for AddEdgeOnly {
    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        self.0.add(world);
    }

    fn backward(&self, world: &mut World, _: &mut UndoCommandManager) {
        self.0.remove(world);
    }
}

/// Adds the GUI representation of an edge. Use `add_edge()` to add an actual edge, which also
/// calls this function.
fn add_gui_edge(world: &mut World, edge: Edge, start_end: Option<(Vec2, Vec2)>) {
    let (start, end) = match start_end {
        Some((start, end)) => (start, end),
        None => {
            let mut start = Vec2::ZERO;
            let mut end = Vec2::ZERO;

            for (slot, global_transform) in world
                .query::<(&SlotAddressSideComponent, &GlobalTransform)>()
                .iter(world)
            {
                if edge.slot_address_output.with_side(Side::Output) == slot.0 {
                    start = global_transform.translation.truncate();
                } else if edge.slot_address_input.with_side(Side::Input) == slot.0 {
                    end = global_transform.translation.truncate();
                }
            }

            (start, end)
        }
    };

    let mut sprite = Sprite {
        color: Color::BLACK,
        ..default()
    };

    let mut transform = Transform::default();

    let color = {
        let engine = world.resource_mut::<Engine>();
        let node_type = engine
            .node_type(edge.slot_address_input.without_slot_id())
            .expect("expecting the node to exist");
        let slot = node_type
            .slot_with_id(Side::Input, edge.slot_address_input.slot_id())
            .expect("tried accessing a slot that does not exist");
        slot_type_to_color(slot.slot_type, 0.7)
    };

    stretch_between(&mut sprite, &mut transform, start, end);

    world
        .spawn()
        .insert_bundle(SpriteBundle {
            sprite,
            transform,
            ..default()
        })
        .insert(GuiEdge {
            start,
            end,
            output_slot_address: edge.slot_address_output,
            input_slot_address: edge.slot_address_input,
        })
        .with_children(|parent| {
            parent
                .spawn_bundle(SpriteBundle {
                    sprite: Sprite {
                        color,
                        custom_size: Some(Vec2::new(2.0, 2.0)),
                        ..default()
                    },
                    transform: Transform::from_translation(Vec3::Z * SMALLEST_DEPTH_UNIT),
                    ..default()
                })
                .insert(EdgeColor);
        });
}

fn remove_gui_edge(world: &mut World, edge: Edge) {
    let mut q_gui_edge = world.query::<(Entity, &GuiEdge)>();

    let edges_to_remove = q_gui_edge
        .iter(world)
        .filter(|(_, gui_edge)| {
            let input_slot = gui_edge.input_slot_address;
            let output_slot = gui_edge.output_slot_address;

            output_slot.node_id() == edge.slot_address_output.node_id()
                && output_slot.slot_id() == edge.slot_address_output.slot_id()
                && input_slot.node_id() == edge.slot_address_input.node_id()
                && input_slot.slot_id() == edge.slot_address_input.slot_id()
        })
        .map(|(entity, _)| entity)
        .collect::<Vec<_>>();

    for entity in edges_to_remove {
        despawn_with_children_recursive(world, entity);
        info!("removed gui edge: {:?}", entity);
    }
}
