// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

pub mod edge;
pub mod gui;
pub mod node;
pub mod prelude;
pub mod undo_command_manager;
pub mod undo_redo_tool;

use self::undo_command_manager::UndoCommandManager;
use bevy::prelude::*;
use std::fmt::Debug;

trait AddRemove: Debug {
    fn add(&self, world: &mut World);
    fn remove(&self, world: &mut World);
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum UndoCommandType {
    Command,
    Custom,
    Checkpoint,
}

pub trait UndoCommand: Debug {
    fn command_type(&self) -> UndoCommandType {
        UndoCommandType::Command
    }
    fn forward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager);
    fn backward(&self, world: &mut World, undo_command_manager: &mut UndoCommandManager);
}
