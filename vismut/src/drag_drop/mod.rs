// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

pub mod edge;
pub mod node;

use crate::node::MoveToCursor;
use crate::{AmbiguitySet, CustomStage, GrabToolType, ToolState};
use bevy::prelude::*;

use self::{
    edge::{grab_edge_cleanup, propagate_frame_visibility, update_edge_color},
    node::{grab_node_setup, grab_node_update_edge},
};
use self::{
    edge::{grab_edge_update, grab_tool_slot_setup},
    node::{grab_node_cleanup, grab_node_update},
};

#[derive(Component, Default)]
pub(crate) struct Draggable;
#[derive(Component, Default)]
pub(crate) struct Dragged {
    start: Vec2,
}
#[derive(Component, Default)]
pub(crate) struct Dropped;

/// This is relevant when the user double-clicks an image in a file dialog when creating an image
/// node. Without it, the release of the double-click will drop the node.
pub enum DragDropMode {
    Released,
    Pressed,
}

pub(crate) struct WorkspaceDragDropPlugin;
impl Plugin for WorkspaceDragDropPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(MoveToCursor(false))
            .insert_resource(DragDropMode::Released)
            .add_system_set_to_stage(
                CoreStage::Update,
                SystemSet::new()
                    .label(CustomStage::Update)
                    .after(CustomStage::Setup)
                    .with_system(
                        grab_node_setup
                            .with_run_criteria(State::on_enter(ToolState::Grab(GrabToolType::Node)))
                            .in_ambiguity_set(AmbiguitySet),
                    )
                    .with_system(
                        grab_node_update
                            .chain(grab_node_update_edge)
                            .with_run_criteria(State::on_update(ToolState::Grab(
                                GrabToolType::Node,
                            )))
                            .in_ambiguity_set(AmbiguitySet),
                    )
                    .with_system(
                        grab_node_cleanup
                            .with_run_criteria(State::on_exit(ToolState::Grab(GrabToolType::Node)))
                            .in_ambiguity_set(AmbiguitySet),
                    ),
            )
            .add_startup_system(edge::setup.in_ambiguity_set(AmbiguitySet))
            .add_system_set_to_stage(
                CoreStage::Update,
                SystemSet::new()
                    .with_system(
                        grab_tool_slot_setup
                            .with_run_criteria(State::on_enter(ToolState::Grab(GrabToolType::Slot)))
                            .in_ambiguity_set(AmbiguitySet),
                    )
                    .with_system(
                        grab_edge_update
                            .with_run_criteria(State::on_update(ToolState::Grab(
                                GrabToolType::Slot,
                            )))
                            .in_ambiguity_set(AmbiguitySet),
                    )
                    .with_system(
                        grab_edge_cleanup
                            .with_run_criteria(State::on_exit(ToolState::Grab(GrabToolType::Slot)))
                            .in_ambiguity_set(AmbiguitySet),
                    )
                    .with_system(propagate_frame_visibility.in_ambiguity_set(AmbiguitySet)),
            )
            .add_system_set_to_stage(
                CoreStage::PostUpdate,
                SystemSet::new().with_system(update_edge_color),
            );
    }
}
