// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

use std::fmt::Debug;

use crate::{
    shared::NodeAddressComponent, stretch_between, sync_graph::GuiEdge, undo::prelude::*, Cursor,
    DragDropMode, Selected, SlotAddressSideComponent, ToolState, NODE_HEIGHT,
};
use bevy::prelude::*;
use vismut_core::prelude::*;

use super::{Dragged, Dropped};

pub struct MoveToCursor(pub bool);

#[derive(Clone, Debug)]
pub struct MoveNodeUndo {
    pub node_address: NodeAddress,
    pub from: Vec2,
    pub to: Vec2,
}

impl UndoCommand for MoveNodeUndo {
    fn forward(&self, world: &mut World, _: &mut UndoCommandManager) {
        let mut query = world.query::<(&NodeAddressComponent, &mut Transform)>();
        if let Some((_, mut transform)) = query
            .iter_mut(world)
            .find(|(node_id, _)| node_id.0 == self.node_address)
        {
            transform.translation.x = self.to.x;
            transform.translation.y = self.to.y;
            update_node_gui_edges(world, self.node_address);
        }
    }

    fn backward(&self, world: &mut World, _: &mut UndoCommandManager) {
        let mut query = world.query::<(&NodeAddressComponent, &mut Transform)>();
        if let Some((_, mut transform)) = query
            .iter_mut(world)
            .find(|(node_id, _)| node_id.0 == self.node_address)
        {
            transform.translation.x = self.from.x;
            transform.translation.y = self.from.y;
            update_node_gui_edges(world, self.node_address);
        }
    }
}

fn update_node_gui_edges(world: &mut World, node_address: NodeAddress) {
    let node_transform = *world
        .query::<(&NodeAddressComponent, &Transform)>()
        .iter(world)
        .find(|(node_id_iter, _)| node_id_iter.0 == node_address)
        .map(|(_, transform)| transform)
        .unwrap();
    let slots = world
        .query::<(&SlotAddressSideComponent, &Transform)>()
        .iter(world)
        .filter(|(slot, _)| slot.0.node_address() == node_address)
        .map(|(slot, transform)| (*slot, *transform))
        .collect::<Vec<(SlotAddressSideComponent, Transform)>>();

    let mut q_edge = world.query::<(&mut Sprite, &mut Transform, &mut GuiEdge)>();

    for (mut sprite, mut edge_t, mut edge) in q_edge.iter_mut(world).filter(|(_, _, edge)| {
        edge.input_slot_address.without_slot_id() == node_address
            || edge.output_slot_address.without_slot_id() == node_address
    }) {
        for (slot, slot_t) in slots.iter() {
            if slot.0 == edge.output_slot_address.with_side(Side::Output) {
                edge.start = (node_transform.translation + slot_t.translation).truncate();
            } else if slot.0 == edge.input_slot_address.with_side(Side::Input) {
                edge.end = (node_transform.translation + slot_t.translation).truncate();
            }
        }

        stretch_between(&mut sprite, &mut edge_t, edge.start, edge.end);
    }
}

/// Grab all selected nodes.
pub(crate) fn grab_node_setup(
    mut commands: Commands,
    mut move_to_cursor: ResMut<MoveToCursor>,
    mut tool_state: ResMut<State<ToolState>>,
    mut q_selected_nodes: Query<
        (Entity, &mut Transform, &GlobalTransform),
        (With<NodeAddressComponent>, With<Selected>),
    >,
    q_cursor: Query<(Entity, &GlobalTransform), With<Cursor>>,
) {
    let (cursor_e, cursor_transform) = q_cursor.single();
    let mut any_nodes = false;

    for (index, (entity, mut transform, global_transform)) in
        q_selected_nodes.iter_mut().enumerate()
    {
        commands.entity(cursor_e).push_children(&[entity]);
        commands.entity(entity).insert(Dragged {
            start: global_transform.translation.truncate(),
        });

        if move_to_cursor.0 {
            const NODE_OFFSET: f32 = NODE_HEIGHT + 12.0;
            transform.translation.x = 0.0;
            transform.translation.y = -NODE_OFFSET * index as f32;
        } else {
            let cursor_space = global_transform.translation - cursor_transform.translation;
            transform.translation.x = cursor_space.x;
            transform.translation.y = cursor_space.y;
        }

        any_nodes = true;
    }

    if move_to_cursor.0 {
        move_to_cursor.0 = false;
    }

    if !any_nodes {
        tool_state.overwrite_replace(ToolState::None).unwrap();
    }
}

/// Exit grab tool if mouse button is released.
pub(crate) fn grab_node_update(
    mut commands: Commands,
    mut undo_command_manager: ResMut<UndoCommandManager>,
    q_dragged: Query<(Entity, &NodeAddressComponent, &Dragged, &GlobalTransform)>,
    mut tool_state: ResMut<State<ToolState>>,
    mut i_mouse_button: ResMut<Input<MouseButton>>,
    mut drag_drop_mode: ResMut<DragDropMode>,
) {
    if let DragDropMode::Pressed = *drag_drop_mode {
        if i_mouse_button.just_pressed(MouseButton::Left) {
            *drag_drop_mode = DragDropMode::Released;
        } else {
            return;
        }
    }

    if i_mouse_button.just_released(MouseButton::Left) {
        for (entity, node_id, dragged, gtransform) in q_dragged.iter() {
            let to = gtransform.translation.truncate();

            undo_command_manager.push(Box::new(MoveNodeUndo {
                node_address: node_id.0,
                from: dragged.start,
                to,
            }));

            commands
                .entity(entity)
                .remove::<Parent>()
                .remove::<Dragged>()
                .insert(Dropped);
        }

        undo_command_manager.push(Box::new(Checkpoint));
        tool_state.overwrite_replace(ToolState::None).unwrap();

        i_mouse_button.clear();
    }
}

pub(crate) fn grab_node_cleanup(
    mut commands: Commands,
    mut q_dragged: Query<
        (Entity, AnyOf<(&Dragged, &Dropped)>, &mut Transform),
        With<NodeAddressComponent>,
    >,
) {
    for (entity, (dragged, dropped), mut transform) in q_dragged.iter_mut() {
        if dragged.is_some() || dropped.is_some() {
            commands.entity(entity).remove::<Dragged>();
            commands.entity(entity).remove::<Parent>();

            if let Some(dragged) = dragged {
                transform.translation.x = dragged.start.x;
                transform.translation.y = dragged.start.y;
            }
        }
    }
}

pub(crate) fn grab_node_update_edge(
    q_node: Query<(&NodeAddressComponent, &Transform), With<Dragged>>,
    q_slot: Query<(&SlotAddressSideComponent, &Transform)>,
    mut q_edge: Query<
        (&mut Sprite, &mut Transform, &mut GuiEdge),
        (
            Without<NodeAddressComponent>,
            Without<SlotAddressSideComponent>,
            Without<Cursor>,
        ),
    >,
    q_cursor: Query<&Transform, With<Cursor>>,
) {
    let cursor_t = q_cursor.iter().next().unwrap().translation;

    for (node_address, node_t) in q_node.iter() {
        for (mut sprite, mut edge_t, mut edge) in q_edge.iter_mut().filter(|(_, _, edge)| {
            edge.input_slot_address.without_slot_id() == node_address.0
                || edge.output_slot_address.without_slot_id() == node_address.0
        }) {
            for (slot, slot_t) in q_slot
                .iter()
                .filter(|(slot, _)| slot.0.node_address() == node_address.0)
            {
                if slot.0 == edge.output_slot_address.with_side(Side::Output) {
                    edge.start = (cursor_t + node_t.translation + slot_t.translation).truncate();
                } else if slot.0 == edge.input_slot_address.with_side(Side::Input) {
                    edge.end = (cursor_t + node_t.translation + slot_t.translation).truncate();
                }
            }

            stretch_between(&mut sprite, &mut edge_t, edge.start, edge.end);
        }
    }
}
