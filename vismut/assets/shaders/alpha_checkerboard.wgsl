// This file is part of Vismut.
// Copyright (C) 2022  Vismut developers
//
// Vismut is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Vismut is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Vismut.  If not, see <https://www.gnu.org/licenses/>.

struct AlphaCheckeboardMaterial {
    scale: f32;
};
[[group(1), binding(0)]]
var<uniform> material: AlphaCheckeboardMaterial;

// Discards the integer and returns only the fraction of a number.
fn frac(in: f32) -> f32 {
    return in - floor(in);
}

[[stage(fragment)]]
fn fragment(
    [[builtin(position)]] in_position: vec4<f32>
) -> [[location(0)]] vec4<f32> {
    let size = f32(8.0 * material.scale);
    let size_fraction = f32(1.0 / size);
    
    var checker = f32(floor(in_position.x * size_fraction) + floor(in_position.y * size_fraction));
    checker = frac(checker * 0.5);
    checker = checker * 2.0;

    checker = clamp(checker, 0.03, 0.1);
    
    return vec4<f32>(checker, checker, checker, 1.0);
}