# Vismut
**Vismut will be a procedural texturing tool for Windows and Linux**, allowing for a 100% non-destructive material creation workflow. This means you build textures from scratch using procedural nodes, allowing for changes to any step in the process at any time, including changing the texture's resolution.

The application's backend **Vismut Core** can be used in for instance a game to generate textures on the client. This can be used for more character customization options, massive game download size reductions, and more.

### [Download](https://orsvarn.com/fileshare/vismut/) | [Community](https://vismut.zulipchat.com) | [Manual](https://gitlab.com/vismut-org/vismut/-/blob/main/MANUAL.md)

![Screenshot](screenshot.png)

## Goals
We use feedback along with our roadmap and design goals to guide our efforts towards the most valuable work.

### Roadmap
The roadmap gives an idea of where the project is headed, it changes with feedback and progress.

1. 🔲 Grid snapping <-- [**v0.6**](https://gitlab.com/vismut-org/vismut/-/milestones/6)
2. 💾 Saving and loading node graphs
3. 🎨 Mix node
4. ❇️ Noise node
5. 📥 Graph node
6. 🔢 Variables
7. 🖼️ 2D preview
8. 🔘 Inline properties panel
9. 📷 3D preview
10. ⚙️ Command-line tool

### Design goals
- [x] ⏱️ **Responsive** - Fast to start, fluid to use
- [x] 😌 **Simple** - Focused feature set, easy to learn, hotkeys for speed
- [ ] ⚡ **Powerful** - Create any material, standardized workflow, fit into any pipeline

## Features
The only use case supported in the current version (v0.5) is **manual channel packing**.

### Nodes
- **Image**: Loads an image from disk
- **Output**: Exports an image to disk
- **Split**: Splits an RGBA image into 4 grayscale images
- **Merge**: Merges 4 grayscale images into an RGBA image
- **Grayscale**: Generates a pixel with the given value

### Other
- Undo & redo

## Contributing
Join the [Vismut Zulip](https://vismut.zulipchat.com) and ping `@**Lukas Orsvärn**`. **I'd be more than happy to get on a video/voice call to show you around the code base and explain how things work**, or help get you started with a task, or discuss an idea or issue, or do pair programming, etc. Close collaboration enables a lot more learning, fun and productivity!

Regardless of how and how much you want to contribute, welcome! 😄

## Libraries Used
Some of the most important libraries Vismut uses are [Bevy](https://github.com/bevyengine/bevy) for the GUI, and [Image](https://github.com/image-rs/image) for loading and saving images.

## Licenses
This project uses a different license for the frontend and the backend. Vismut and Vismut Core's licenses can be found in their respective directories.

The backend library **Vismut Core**, which is what you'd integrate into your own project, is licensed under `Apache-2.0 or MIT`. This is the standard license in the Rust ecosystem.

The frontend **Vismut**, which is simply a GUI for Vismut Core, is licensed under `GPL-3.0`. This license is not intended to stop other open source projects from using the code, so I'm open to providing access to the frontend code under a permissive license if that would be helpful.